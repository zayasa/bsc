<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Laporan Scorecard<small> laporan Scorecard.</small></h3>
		<hr class="row-title">
		<div class="row">
			<form class="form-horizontal" name="f1" id="f1" action="/laporan-scorecard/tampil" method="POST">
				<div class="form-group">
					<label for="inpsingkatan" class="col-sm-2 control-label">Periode</label>
					<div class="col-sm-5">
						<?=$tpl->selectList("kode_periode","periode","kode_periode","kode_periode,nama_periode",$kode_periode,"  onChange=\"test()\" "," ")?>
					</div>
				</div>
				<div class="form-group">
					<label for="txtdeskripsi" class="col-sm-2 control-label">Tanggal mulai</label>
					<div class="col-sm-3">
						<input type="date" class="form-control"  name="tanggal_mulai" value="<?=$tanggal_mulai?>">
					</div>
				</div>
				<div class="form-group">
					<label for="txtdeskripsi" class="col-sm-2 control-label">Tanggal selesai</label>
					<div class="col-sm-3">
						<input type="date" class="form-control" name="tanggal_selesai" value="<?=$tanggal_selesai?>">
					</div>
				</div>
				<div class="form-group">
					<label for="inpsingkatan" class="col-sm-2 control-label">Unit kerja</label>
					<div class="col-sm-5">
						<?=$tpl->selectList("kode_unit_kerja","unit_kerja","kode_unit_kerja","nama_unit_kerja",$kode_induk_unit_kerja,""," WHERE tanggal_mulai_unit_kerja<=SYSDATE() AND (tanggal_selesai_unit_kerja IS NULL OR tanggal_selesai_unit_kerja>=SYSDATE()) ORDER BY kode_unit_kerja,kode_induk_unit_kerja,level_unit_kerja ASC ")?>
					</div>
				</div>
				<div class="form-group">
					<label for="inplevel" class="col-sm-2 control-label">keluaran</label>
					<div class="col-sm-2">
						<select class="form-control" name="keluaran">
						<option value='html'>html</option>
						<option value='excel'>excel</option>
						</select>
					</div>
				</div>
				<center><button class="btn btn-primary">generate</button></center>
			</form>
			<p class="text-muted">(*) harus diisi</p>
		</div>
	</div>
</div>
<script>
function test(){
}
</script>
