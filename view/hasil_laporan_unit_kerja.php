<?php
$header_page = "";
$footer_page = "";
if($keluaran=="excel"){
	$contents = ob_get_contents();
	ob_end_clean();
	
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=laporan unit kerja.xls");	
}
else{
	$header_page = "
<div class=\"panel panel-default borderless\">
	<div class=\"panel-body\">
				   ";
	$footer_page = "		
	</div>
</div>
				   ";
}
echo $header_page;
?>
		<table class="table table-hover">
			<tr>
				<th>No.</th>
				<th>Entitas</th>
				<th>Kode Unit Kerja</th>
				<th>Nama Unit Kerja</th>
				<th>Singkatan Unit Kerja</th>
				<th>Deskripsi Unit Kerja</th>
				<th>Induk Unit Kerja</th>
				<th>Tanggal Mulai Unit Kerja</th>
				<th>Tanggal Selesai Unit Kerja</th>
			</tr>
			<?php 
				$no = $start;
				foreach($rs_data AS $data){
					$no++;
					if(is_array($data)){
						foreach($data AS $key=>$val){
							$key  = strtolower($key);
							if(preg_match("/tanggal|tgl/i",$key) && $val!="")
								$val = date("d/m/Y",strtotime($val));
							
							$$key = trim($val);							
						}
					}
					
					echo "
						<tr class=\"odd gradeX\">
							<td>".$no."</td>
							<td>".$nama_entitas."</td>
							<td>".$kode_unit_kerja."</td>
							<td>".$nama_unit_kerja."</td>
							<td>".$singkatan_unit_kerja."</td>
							<td>".$deskripsi_unit_kerja."</td>
							<td>".$nama_induk_unit_kerja."</td>
							<td>".$tanggal_mulai_unit_kerja."</td>
							<td>".$tanggal_selesai_unit_kerja."</td>
						</tr>
						 ";
				}
				if($no<1){
					echo "
						<tr class=\"odd gradeX\">
							<td colspan=\"7\" class=\"center\">** TIDAK ADA DATA **</td>
						</tr>
						 ";					
				}
			?>			
		</table>
<?=$footer_page?>