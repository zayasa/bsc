<link rel='StyleSheet' href='/assets/css/jquery.treegrid.css' type='text/css' />
<script type="text/javascript" src="/assets/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/assets/js/jquery.treegrid.min.js"></script>


<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Scorcard Pegawai <small> Form tambah/ubah scorecard pegawai.</small></h3>
		<hr class="row-title">
		<div class="row">
			<form class="form-horizontal" name="f1" id="f1" action="/pengguna/<?=$_action?>" method="POST">
				<div class="form-group">
					<label for="inpkode" class="col-sm-2 control-label">Kode Pegawai *</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="inpkode" name="kode_pengguna" maxLength="10"  <?=$readonly;?> value="<?=$kode_pengguna?>" >
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Nama Pegawai *</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="inpnama" name="nama_pengguna" value="<?=$nama_pengguna?>" disabled>
					</div>
				</div>
				<div class="form-group">
					<label for="inpentitas" class="col-sm-2 control-label">Entitas *</label>
					<div class="col-sm-4">
						<?=$tpl->selectList("kode_entitas_pengguna","entitas","kode_entitas","nama_entitas",$kode_entitas_pengguna," disabled")?>
					</div>
				</div>
				<div class="form-group">
					<label for="inpunitkerja" class="col-sm-2 control-label">Unit Kerja *</label>
					<div class="col-sm-4">
						<?=$tpl->selectList("kode_unit_kerja_pengguna","unit_kerja","kode_unit_kerja","nama_unit_kerja",$kode_unit_kerja_pengguna,"disabled")?>
					</div>
				</div>
				<div class="form-group">
					<label for="inpjabatan" class="col-sm-2 control-label">Jabatan *</label>
					<div class="col-sm-4">
						<?=$tpl->selectList("kode_jabatan_pengguna","jabatan","kode_jabatan","nama_jabatan",$kode_jabatan_pengguna,"disabled")?>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<table class="table table-hover tree">
							<tbody id="table-loaded">
							</tbody>
						</table>						
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
$.ajax({
	method: "GET",
	url: "/ajax/get_scorecard.php",
	data: {kode_unit_kerja : "<?=$kode_pengguna?>",kode_periode : "<?=$kode_periode?>" , tgl_mulai : "<?=$tanggal_mulai_view?>", tgl_selesai : "<?=$tanggal_selesai_view?>",controller : "<?=$controller?>"},
	cache: false,
	async: false,
	beforeSend: function( xhr ) {
		$( "#table-loaded" ).html('<img src="/assets/img/loading.gif"> loading ..');
	}
})
.done(function( data ) {
	$( "#table-loaded" ).html( data );
	$('.tree').treegrid({
		'initialState': 'collapsed',
		'saveState': true,		
	});
});

</script>