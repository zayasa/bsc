<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Unit Kerja <small> Form tambah/ubah unit kerja .</small></h3>
		<hr class="row-title">
		<div class="row">
			<form class="form-horizontal" name="f1" id="f1" action="/unit-kerja/<?=$_action?>" method="POST">
				<div class="form-group">
					<label for="inpentitas" class="col-sm-2 control-label">Entitas *</label>
					<div class="col-sm-4">
						<?=$tpl->selectList("kode_entitas","entitas","kode_entitas","nama_entitas",$kode_entitas)?>
					</div>
				</div>
				<div class="form-group">
					<label for="inplevel" class="col-sm-2 control-label">Level *</label>
					<div class="col-sm-1">
						<select class="form-control" name="level_unit_kerja" id="inplevel">
						<option></option>
						<?php
							for($a=1;$a<=5;$a++){
								echo "<option value=\"".$a."\" ".($a==$level_unit_kerja ? "selected " : "").">".$a."</option>";
							}
						?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inpinduk" class="col-sm-2 control-label">Induk Unit Kerja</label>
					<div class="col-sm-4">
						<?=$tpl->selectList("kode_induk_unit_kerja","unit_kerja","kode_unit_kerja","nama_unit_kerja",$kode_induk_unit_kerja,""," WHERE kode_unit_kerja<>'".$kode_unit_kerja."' ")?>
					</div>
				</div>
				<div class="form-group">
					<label for="inpkode" class="col-sm-2 control-label">Kode Unit Kerja *</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="inpkode" name="kode_unit_kerja" maxLength="10"  <?=$readonly;?> value="<?=$kode_unit_kerja?>" >
					</div>
				</div>
				<div class="form-group">
					<label for="inpsingkatan" class="col-sm-2 control-label">Singkatan Unit Kerja</label>
					<div class="col-sm-1">
						<input type="text" class="form-control" id="inpsingkatan" name="singkatan_unit_kerja" value="<?=$singkatan_unit_kerja?>" maxLength="4" >
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Nama Unit Kerja *</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="inpnama" name="nama_unit_kerja" value="<?=$nama_unit_kerja?>">
					</div>
				</div>
				<div class="form-group">
					<label for="txtdeskripsi" class="col-sm-2 control-label">Deskripsi </label>
					<div class="col-sm-8">
						<textarea class="form-control" id="txtdeskripsi" name="deskripsi_unit_kerja" rows="3"><?=$deskripsi_unit_kerja?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="inptanggalmulai" class="col-sm-2 control-label">Tanggal mulai *</label>
					<div class="col-sm-2">
						<input type="date" class="form-control" id="inptanggalmulai" name="tanggal_mulai_unit_kerja" value="<?=$tanggal_mulai_unit_kerja?>">
					</div>
				</div>
				<div class="form-group">
					<label for="inptanggalselesai" class="col-sm-2 control-label">Tanggal Selesai </label>
					<div class="col-sm-2">
						<input type="date" class="form-control" id="inptanggalselesai" name="tanggal_selesai_unit_kerja" value="<?=$tanggal_selesai_unit_kerja?>">
					</div>
				</div>
				<center><a href="/unit-kerja" class="btn btn-warning">kembali</a> <button class="btn btn-primary">Simpan</button></center>
			</form>
			<p class="text-muted">(*) harus diisi</p>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	
	$("#f1").validate({
		ignore: "",
		rules: {
			kode_entitas: {
				required: true
			},
			level_unit_kerja: {
				required: true
			},
			kode_unit_kerja: {
				required: true,
				minlength: 10,
				maxlength: 10
			},
			nama_unit_kerja: {
				required: true
			},
			tanggal_mulai_unit_kerja: {
				required: true
			}
		},
		messages: {
			kode_entitas: {
				required: "Pilih entitas!"
			},
			level_unit_kerja: {
				required: "Pilih level!"
			},
			kode_unit_kerja: {
				required: "Masukkan kode unit kerja!",
				minlength: "kode unit kerja 10 huruf!",
				maxlength: "kode unit kerja 10 huruf!"
			},
			nama_unit_kerja: {
				required: "Masukkan nama unit kerja!"
			},
			tanggal_mulai_unit_kerja: {
				required: "Masukkan tanggal mulai unit kerja!"
			}
		}
	});
	
});	
</script>