<link rel='StyleSheet' href='/assets/component/dtree/dtree.css' type='text/css' />
<script type="text/javascript" src="/assets/component/dtree/dtree.js"></script>


<link rel='StyleSheet' href='/assets/css/jquery.treegrid.css' type='text/css' />
<script type="text/javascript" src="/assets/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/assets/js/jquery.treegrid.min.js"></script>

<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Scorecard Unit Kerja <small> scorecard untuk unit kerja.</small></h3>
		<hr class="row-title">
		<div class="row">
			<form class="form-horizontal" name="f1" id="f1" action="/scorecard-unit-kerja/<?=$kode_periode?>" method="POST">
				<div class="form-group">
					<label for="inpkode" class="col-sm-2 control-label text-left">Periode</label>
					<div class="col-sm-3">
						<input type="date" class="form-control" name="tanggal_mulai_view" min="<?=$l_periode['tanggal_mulai_periode']?>" max="<?=$l_periode['tanggal_selesai_periode']?>" value="<?=$tanggal_mulai_view?>">
					</div>
					<div class="col-sm-3">
						<input type="date" class="form-control" name="tanggal_selesai_view" min="<?=$l_periode['tanggal_mulai_periode']?>" max="<?=$l_periode['tanggal_selesai_periode']?>"  value="<?=$tanggal_selesai_view?>">
					</div>					
					<div class="col-sm-3">
						<button type="submit" class="btn btn-default">lihat</button>
					</div>
				</div>
			</form>
		</div>
		<div class="row">
			<div class="col-md-4 dtree">
				<div class="panel panel-default">
					<div class="panel-heading">Unit Kerja</div>
					<div class="panel-body dtree">
						<script type="text/javascript">
						<!--
						d = new dTree('d','kategori_training.php');
						<?=$txt_tree;?>
						document.write(d);
						</script>					
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading">Scorecard</div>
					<div class="panel-body" style="width:100%;height:100%;min-heigth:400px;">
						<table class="table table-hover tree">
							<tbody id="table-loaded">
							</tbody>
						</table>	  					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function test(param){
	$.ajax({
		method: "GET",
		url: "/ajax/get_scorecard.php",
		data: {kode_unit_kerja : param,kode_periode : "<?=$kode_periode?>" , tgl_mulai : "<?=$tanggal_mulai_view?>", tgl_selesai : "<?=$tanggal_selesai_view?>",controller : "<?=$controller?>"},
		cache: false,
		async: false,
		beforeSend: function( xhr ) {
			$( "#table-loaded" ).html('<img src="/assets/img/loading.gif"> loading ..');
		}
	})
	.done(function( data ) {
		$( "#table-loaded" ).html( data );
		$('.tree').treegrid({
			'initialState': 'collapsed',
			'saveState': true,		
		});
	});
}
$(document).ready(function() {
	$('.tree').treegrid({
		'initialState': 'collapsed',
		'saveState': true,		
	});
});
</script>
<style>
.tree tbody tr td {
	padding:2px;
}
.row-container{
	display:inline-block;
	position: relative;
	width:80%;
}
.row-container img {
	display:inline-block;
}
.span_container{
	display:inline-block;
	position: relative;
	width:90%;
}
</style>
