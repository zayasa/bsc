                <?php 
				
					if(in_array("aberanda",$hak_akses_menu)){
				?>
				<li>
                    <a href="/home" id="aberanda">Beranda</a>
                </li>
                <?php 
					}
					if(in_array("ascorecard",$hak_akses_menu)){
				?>
                <li data-toggle="collapse" data-target="#scorecard" class="collapsed active">
                    <a href="javascript:void(0)" id="ascorecard">Scorecard <span class="caret"></span></a>
                </li>
                <?php 
					}
				?>
                <ul class="sub-menu collapse" id="scorecard">
                <?php 
					if(in_array("aperiode-scorecard",$hak_akses_menu)){
				?>
                    <li ><a href="/periode-scorecard" id="aperiode-scorecard">Periode Scorecard</a></li>
                <?php 
					}
					if(in_array("ascorecard-unit-kerja",$hak_akses_menu)){
				?>
                    <li><a href="/scorecard-unit-kerja" id="ascorecard-unit-kerja">Scorecard Unit Kerja</a></li>
                <?php 
					}
					if(in_array("ascorecard-pegawai",$hak_akses_menu)){
				?>
                    <li><a href="/scorecard-pegawai" id="ascorecard-pegawai">Scorecard Pegawai</a></li>
                <?php 
					}
				?>
                </ul>
                <?php 
					if(in_array("alaporan",$hak_akses_menu)){
				?>
                <li data-toggle="collapse" data-target="#laporan" class="collapsed active">
                    <a href="javascript:void(0)" id="alaporan">Laporan <span class="caret"></span></a>
                </li>
                <?php 
					}
				?>
                <ul class="sub-menu collapse" id="laporan">
                <?php 
					if(in_array("aringkasan-scorecard",$hak_akses_menu)){
				?>
                    <li><a href="/ringkasan-scorecard" id="aringkasan-scorecard">Ringkasan Scorecard</a></li>
                <?php 
					}
					if(in_array("alaporan-scorecard",$hak_akses_menu)){
				?>
                    <li><a href="/laporan-scorecard" id="alaporan-scorecard">Laporan Scorecard</a></li>
                <?php 
					}
					if(in_array("alaporan-unit-kerja",$hak_akses_menu)){
				?>
                    <li><a href="/laporan-unit-kerja" id="alaporan-unit-kerja">Laporan Unit Kerja</a></li>
                <?php 
					}
					if(in_array("alaporan-pegawai",$hak_akses_menu)){
				?>
                    <li><a href="/laporan-pegawai" id="alaporan-pegawai">Laporan Pegawai</a></li>
                <?php 
					}
				?>
                </ul>
                <?php 
					if(in_array("apengaturan",$hak_akses_menu)){
				?>
                <li data-toggle="collapse" data-target="#pengaturan" class="collapsed active">
                    <a href="javascript:void(0)" id="apengaturan">Pengaturan <span class="caret"></span></a>
                </li>
                <?php 
					}
				?>
                <ul class="sub-menu collapse" id="pengaturan">
                <?php 
					if(in_array("aentitas",$hak_akses_menu)){
				?>
                    <li><a href="/entitas" id="aentitas">Entitas</a></li>
                <?php 
					}
					if(in_array("aunit-kerja",$hak_akses_menu)){
				?>
                    <li><a href="/unit-kerja" id="aunit-kerja">Unit Kerja</a></li>
                <?php 
					}
					if(in_array("ajabatan",$hak_akses_menu)){
				?>
                    <li><a href="/jabatan" id="ajabatan">Jabatan</a></li>
                <?php 
					}
					if(in_array("apengguna",$hak_akses_menu)){
				?>
                    <li><a href="/pengguna" id="apengguna">Pengguna</a></li>
                <?php 
					}
					if(in_array("ahak-akses",$hak_akses_menu)){
				?>
                    <li><a href="/hak-akses" id="ahak-akses">hak akses</a></li>
                <?php 
					}
				?>
                </ul>
