<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Profil saya <small> informasi pribadi saya.</small></h3>
		<hr class="row-title">
		<form name="f1" id="f1" action="/profil-saya/update" method="POST" class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Kode Pengguna</label>
				<div class="col-sm-10">
					<p class="form-control-static"><?php echo $detail_login['kode_pengguna'] ?></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Nama Pengguna</label>
				<div class="col-sm-10">
					<p class="form-control-static"><?php echo $detail_login['nama_pengguna'] ?></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Email Pengguna</label>
				<div class="col-sm-10">
					<p class="form-control-static"><?php echo $detail_login['email_pengguna'] ?></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Unit Kerja</label>
				<div class="col-sm-10">
					<p class="form-control-static"><?php echo $detail_login['nama_unit_kerja'] ?></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Jabatan</label>
				<div class="col-sm-10">
					<p class="form-control-static"><?php echo $detail_login['nama_jabatan'] ?></p>
				</div>
			</div>
			<hr class="row-title">
			<div class="form-group">
				<label class="col-sm-2 control-label">Sandi</label>
				<div class="col-sm-4">
					<input type="password" name="o_password" id="o_password" class="form-control" >
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Sandi Baru</label>
				<div class="col-sm-4">
					<input type="password" name="n_password" id="n_password" class="form-control" >
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Konfirmasi</label>
				<div class="col-sm-4">
					<input type="password" name="k_password" id="k_password" class="form-control" >
				</div>
			</div>
			<center><button class="btn btn-primary">Ubah Sandi</button></center>
		</form>
	</div>
</div>
<script>
$(document).ready(function(){
	
	$("#f1").validate({
		ignore: "",
		rules: {
			o_password: {
				required: true
			},
			n_password: {
				required: true,
				minlength: 4,
				maxlength: 8
			},
			k_password: {
				equalTo: "#n_password"
			}
		},
		messages: {
			o_password: {
				required: "Masukkan Sandi anda!"
			},
			n_password: {
				required: "Masukkan sandi baru anda!",
				minlength: "Sandi minimal 4 huruf!",
				maxlength: "Sandi maksimal 8 huruf!"
			},
			k_password:{
				equalTo: "Ulangi sandi baru anda"
			}
		}
	});
	
});	
</script>