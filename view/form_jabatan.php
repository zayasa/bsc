<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Jabatan <small> Form tambah/ubah jabatan .</small></h3>
		<hr class="row-title">
		<div class="row">
			<form class="form-horizontal" name="f1" id="f1" action="/jabatan/<?=$_action?>" method="POST">
				<div class="form-group">
					<label for="inplevel" class="col-sm-2 control-label">Level *</label>
					<div class="col-sm-1">
						<select class="form-control" name="level_jabatan" id="inplevel">
						<option></option>
						<?php
							for($a=1;$a<=5;$a++){
								echo "<option value=\"".$a."\" ".($a==$level_jabatan ? "selected " : "").">".$a."</option>";
							}
						?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inpinduk" class="col-sm-2 control-label">Induk Jabatan</label>
					<div class="col-sm-4">
						<?=$tpl->selectList("kode_induk_jabatan","jabatan","kode_jabatan","nama_jabatan",$kode_induk_jabatan,""," WHERE kode_jabatan<>'".$kode_jabatan."' ")?>
					</div>
				</div>
				<div class="form-group">
					<label for="inpkode" class="col-sm-2 control-label">Kode Jabatan *</label>
					<div class="col-sm-1">
						<input type="text" class="form-control" id="inpkode" name="kode_jabatan" maxLength="3"  <?=$readonly;?> value="<?=$kode_jabatan?>" >
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Nama Jabatan *</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="inpnama" name="nama_jabatan" value="<?=$nama_jabatan?>">
					</div>
				</div>
				<div class="form-group">
					<label for="txtdeskripsi" class="col-sm-2 control-label">Deskripsi </label>
					<div class="col-sm-8">
						<textarea class="form-control" id="txtdeskripsi" name="deskripsi_jabatan" rows="3"><?=$deskripsi_jabatan?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="inptanggalmulai" class="col-sm-2 control-label">Tanggal mulai *</label>
					<div class="col-sm-2">
						<input type="date" class="form-control" id="inptanggalmulai" name="tanggal_mulai_jabatan" value="<?=$tanggal_mulai_jabatan?>">
					</div>
				</div>
				<div class="form-group">
					<label for="inptanggalselesai" class="col-sm-2 control-label">Tanggal Selesai </label>
					<div class="col-sm-2">
						<input type="date" class="form-control" id="inptanggalselesai" name="tanggal_selesai_jabatan" value="<?=$tanggal_selesai_jabatan?>">
					</div>
				</div>
				<center><a href="/jabatan" class="btn btn-warning">kembali</a> <button class="btn btn-primary">Simpan</button></center>
			</form>
			<p class="text-muted">(*) harus diisi</p>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	
	$("#f1").validate({
		ignore: "",
		rules: {
			level_jabatan: {
				required: true
			},
			kode_jabatan: {
				required: true,
				minlength: 3,
				maxlength: 3
			},
			nama_jabatan: {
				required: true
			},
			tanggal_mulai_jabatan: {
				required: true
			}
		},
		messages: {
			level_jabatan: {
				required: "Pilih level!"
			},
			kode_jabatan: {
				required: "Masukkan kode jabatan!",
				minlength: "kode jabatan 3 huruf!",
				maxlength: "kode jabatan 3 huruf!"
			},
			nama_jabatan: {
				required: "Masukkan nama jabatan!"
			},
			tanggal_mulai_jabatan: {
				required: "Masukkan tanggal mulai jabatan!"
			}
		}
	});
	
});	
</script>