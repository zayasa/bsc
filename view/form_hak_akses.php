<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Hak akses <small> Form tambah/ubah hak akses .</small></h3>
		<hr class="row-title">
		<div class="row">
			<form class="form-horizontal" name="f1" id="f1" action="/hak-akses/<?=$_action?>" method="POST">
				<div class="form-group">
					<label for="inpkode" class="col-sm-2 control-label">Kode Hak akses *</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="inpkode" name="kode_hak" maxLength="10"  <?=$readonly;?> value="<?=$kode_hak?>" >
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Nama Hak akses *</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="inpnama" name="nama_hak" value="<?=$nama_hak?>">
					</div>
				</div>
				<div class="form-group">
					<label for="txtdeskripsi" class="col-sm-2 control-label">Deskripsi </label>
					<div class="col-sm-8">
						<textarea class="form-control" id="txtdeskripsi" name="deskripsi_hak" rows="3"><?=$deskripsi_hak?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="txtdeskripsi" class="col-sm-2 control-label">Menu </label>
					<div class="col-sm-4">
						<table class="table table-bordered">
							<tr>
								<th>Menu</th>
								<th>Akses</th>
							</tr>
							<?php 
								foreach($array_menu AS $key=>$val){
									$menu = str_replace("-"," ",substr($key,1));
									echo "
										<tr>
											<td>".$menu."</td>
											<td class=\"center\"><input type=\"checkbox\" name=\"menu[]\" ".(in_array($key,$rs_menu) ? "checked" : "")." value=\"".$key."\"> </td>
										</tr>
										 ";
								}
							?>
						</table>
					</div>
				</div>
				<center><a href="/hak-akses" class="btn btn-warning">kembali</a> <button class="btn btn-primary">Simpan</button></center>
			</form>
			<p class="text-muted">(*) harus diisi</p>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	
	$("#f1").validate({
		ignore: "",
		rules: {
			kode_hak: {
				required: true,
				maxlength: 10
			},
			nama_hak: {
				required: true
			},
		},
		messages: {
			kode_hak: {
				required: "Masukkan kode hak akses!",
				maxlength: "kode hak akses 10 huruf!"
			},
			nama_hak: {
				required: "Masukkan nama hak akses!"
			}
		}
	});
	
});	
</script>