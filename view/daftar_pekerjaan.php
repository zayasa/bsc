<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Daftar Pekerjaan</h3>
		<hr class="row-title">
		<form class="form-inline" method="POST" action="/daftar-pekerjaan/simpan">
		<table class="table table-hover">
			<tr>
				<th>No.</th>
				<th>Periode</th>
				<th>Scorecard</th>
				<th>Pengguna</th>
				<th>Periode</th>
				<th>Target</th>
				<th>Realisasi</th>
				<th>Skor</th>
				<th>Min</th>
				<th>Max</th>
				<th>Base</th>
				<th>&nbsp;</th>
			</tr>
			<?php 
				$no = $start;
				foreach($rs_data AS $data){
					$no++;
					if(is_array($data)){
						foreach($data AS $key=>$val){
							$key  = strtolower($key);
							if(preg_match("/tanggal|tgl/i",$key))
								$val = date("d/m/Y",strtotime($val));
							$$key = trim($val);							
						}
					}
					
					
					echo "
						<tr class=\"odd gradeX\">
							<td>".$no."</td>
							<td>".$nama_periode."</td>
							<td>".$nama_scorecard."</td>
							<td>".$nama_pengguna."</td>
							<td>".$tanggal_mulai." s/d ".$tanggal_selesai."</td>
							<td>".$_tar."</td>
							<td>".$_ral."</td>
							<td>".$_sco."</td>
							<td>".$_min."</td>
							<td>".$_max."</td>
							<td>".$_bse."</td>
							<td class=\"center\">
								<input class=\"form-control \" type=\"checkbox\" name=\"idd[]\" value=\"".$kode_scorecard."|".$id_periode_waktu."\">
							</td>
						</tr>
						 ";
				}
				if($no<1){
					echo "
						<tr class=\"odd gradeX\">
							<td colspan=\"4\" class=\"center\">** TIDAK ADA DATA **</td>
						</tr>
						 ";					
				}
			?>			
		</table>
		<center><a href="/home" class="btn btn-warning">kembali</a> <button class="btn btn-primary">Setuju</button></center>
		</form>
	</div>
</div>