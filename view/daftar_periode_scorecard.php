<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Periode Scorecard <small> Daftar periode scorecard.</small></h3>
		<hr class="row-title">
		<div class="row">
			<div class="col-md-12">
				<p>Silahkan pilih periode yang anda kehendaki.</p>
				<p class="text-center">
					<center>
						<?php 
							foreach($rs_data AS $data){
								foreach($data AS $key=>$val){
									$key  = strtolower($key);
									
									if(preg_match("/tanggal|tgl/i",$key) && $val!="")
										$val = date("d/m/Y",strtotime($val));
									
									$$key = trim($val);
								}
								echo "
									<a href=\"".$controller."/".$kode_periode."/\">
									<div class=\"col-md-3 btn btn-primary\" style=\"min-height:100px;\">
										<h3>".$nama_periode."</h3>
										<small class=\"pull-right\">".$tanggal_mulai_periode." s/d ".$tanggal_selesai_periode."</small>
									</div>
									</a>
									<div class=\"col-md-1\">
									</div>
									";
							}
						?>
					</center>
				</p>
			</div>
		</div>
	</div>
</div>