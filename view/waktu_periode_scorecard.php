<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Periode <small> daftar waktu periode scorecard.</small></h3>
		<hr class="row-title">
		<div class="row">
			<form class="form-horizontal" >
				<div class="form-group">
					<label for="inpkode" class="col-sm-2 control-label">Kode Periode</label>
					<div class="col-sm-10">
						<p class="form-control-static"><?=$kode_periode?></p>
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Nama Periode</label>
					<div class="col-sm-10">
						<p class="form-control-static"><?=$nama_periode?></p>
					</div>
				</div>
				<div class="form-group">
					<label for="inptanggalmulai" class="col-sm-2 control-label">Tanggal mulai</label>
					<div class="col-sm-10">
						<p class="form-control-static"><?=$tanggal_mulai_periode?> s/d <?=$tanggal_selesai_periode?></p>
					</div>
				</div>
			</form>
		</div>
		<hr class="row-title">
		<br>
		<table class="table table-hover">
			<tr>
				<th>No.</th>
				<th>Kode Satuan waktu</th>
				<th>Satuan waktu</th>
				<th>&nbsp;</th>
			</tr>
			<?php 
				$no = $start;
				foreach($rs_data AS $data){
					$no++;
					if(is_array($data)){
						foreach($data AS $key=>$val){
							$key  = strtolower($key);
							if(preg_match("/tanggal|tgl/i",$key) && $val!="")
								$val = date("d/m/Y",strtotime($val));
							
							$$key = trim($val);							
						}
					}
					if(!empty($tanggal_selesai_periode) && $tanggal_selesai_periode<=date("d/m/Y")){
						$class = "btn-danger";
						$status = "non-aktif";
					}
					else{
						$class = "btn-success";
						$status = "aktif";
					}
					
					echo "
						<tr class=\"odd gradeX\">
							<td>".$no."</td>
							<td>".$kode_satuan_waktu."</td>
							<td>".$nama_satuan_waktu."</td>
							<td class=\"center\">
								<a href=\"/waktu-periode-scorecard/".$kode_periode."/".$kode_satuan_waktu."/\" title=\"daftar periode\"><i class=\"glyphicon glyphicon-calendar\"></i></a> 
							</td>
						</tr>
						 ";
				}
				if($no<1){
					echo "
						<tr class=\"odd gradeX\">
							<td colspan=\"6\" class=\"center\">** TIDAK ADA DATA **</td>
						</tr>
						 ";					
				}
			?>			
		</table>
	</div>
</div>