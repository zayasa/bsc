<div class="panel panel-default borderless">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-9">
				<div class="alert alert-info" role="alert">
					<b><i class="glyphicon glyphicon-bullhorn"></i> Daftar pekerjaan</b><br>
					<?php 
						if($rs_total>=1){
							echo "<a href=\"/daftar-pekerjaan\" >".$rs_total." persetujuan realisasi</a>";
						}
						else{
							echo "Tidak ada persetujuan realisasi";
						}
					?>
					
				</div>
			</div>
			<div class="col-md-3">
				<div class="panel panel-default">
					<div class="panel-heading">Profil Saya</div>
						<div class="panel-body">
							<center><img src="/assets/img/default-profile.png" style="width:140px;" alt="" class="img-circle"><center>
							<span class="block" >&nbsp;</span>
							<span class="block" ><?php echo $detail_login['nama_pengguna'] ?></span>
							<span class="block"><?php echo $detail_login['kode_pengguna'] ?></span>
							<span class="block"><?php echo $detail_login['nama_unit_kerja'] ?></span>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>