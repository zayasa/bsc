<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Pengguna <small> daftar pengguna.</small></h3>
		<hr class="row-title">
		<div class="row">
			<div class="col-md-4">
				<form class="form-inline" method="POST" action="/pengguna">
					<div class="input-group">
						<input type="text" class="form-control input-sm" name="keyword" value="<?=$_key?>" placeholder="Search for...">
						<span class="input-group-btn">
							<button class="btn btn-default btn-sm" type="submit"><i class="glyphicon glyphicon-search"></i></button>
						</span>
					</div><!-- /input-group -->
				</form>			
			</div>
			<div class="col-md-4">
			</div>
			<div class="col-md-4">
				<a href="/pengguna/tambah" class="btn btn-primary  pull-right btn-sm"><i class="glyphicon glyphicon-plus"></i> </a>
			</div>
		</div>
		<br>
		<table class="table table-hover">
			<tr>
				<th>No.</th>
				<th>Kode Pengguna</th>
				<th>Nama Pengguna</th>
				<th>Entitas</th>
				<th>Unit Kerja</th>
				<th>Jabatan</th>
				<th>Tanggal Masuk</th>
				<th>Status</th>
				<th>&nbsp;</th>
			</tr>
			<?php 
				$no = $start;
				foreach($rs_data AS $data){
					$no++;
					if(is_array($data)){
						foreach($data AS $key=>$val){
							$key  = strtolower($key);
							if(preg_match("/tanggal|tgl/i",$key) && $val!="")
								$val = date("d/m/Y",strtotime($val));
							
							$$key = trim($val);							
						}
					}
					
					if((!empty($tanggal_keluar_pengguna) && $tanggal_keluar_pengguna<=date("d/m/Y")) || $status_pengguna=="0"){
						$class = "btn-danger";
						$status = "non-aktif";
					}
					else if($status_pengguna=="2"){
						$class = "btn-warning";
						$status = "ditangguhkan";
					}
					else{
						$class = "btn-success";
						$status = "aktif";
					}
					
					echo "
						<tr class=\"odd gradeX\">
							<td>".$no."</td>
							<td>".$kode_pengguna."</td>
							<td>".$nama_pengguna."</td>
							<td>".$nama_entitas."</td>
							<td>".$nama_unit_kerja."</td>
							<td>".$nama_jabatan."</td>
							<td>".$tanggal_masuk_pengguna."</td>
							<td><button class=\"btn ".$class." btn-indicator\" title=\"".$status."\">&nbsp;</button></td>
							<td class=\"center\">
								<a href=\"/pengguna/ubah/".$kode_pengguna."\"><i class=\"glyphicon glyphicon-pencil\"></i></a>
								&nbsp;&nbsp;&nbsp;<a data-toggle=\"modal\" data-target=\"#confirm-delete\"><i class=\"glyphicon glyphicon-remove\"></i></a> 
								&nbsp;&nbsp;&nbsp;<a href=\"/pengguna/reset/".$kode_pengguna."\" title=\"reset sandi\"><i class=\"glyphicon glyphicon-erase\"></i></a> 
							</td>
						</tr>
						 ";
				}
				if($no<1){
					echo "
						<tr class=\"odd gradeX\">
							<td colspan=\"9\" class=\"center\">** TIDAK ADA DATA **</td>
						</tr>
						 ";					
				}
			?>			
		</table>
		<?php echo $tpl->paging($count_page,$page,$start,"/pengguna",($_key=="" ? "" : "/".$_key))?>
	</div>
</div>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <b>Konfirmasi</b>
            </div>
            <div class="modal-body">
                Apakah anda berhasil menghapus data ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>
<script>
$('#confirm-delete').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', '/pengguna/hapus/<?=$kode_pengguna?>');
});
</script>