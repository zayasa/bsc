<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Entitas <small> Form tambah/ubah entitas .</small></h3>
		<hr class="row-title">
		<div class="row">
			<form class="form-horizontal" name="f1" id="f1" action="/entitas/<?=$_action?>" method="POST">
				<div class="form-group">
					<label for="inpkode" class="col-sm-2 control-label">Kode entitas *</label>
					<div class="col-sm-1">
						<input type="text" class="form-control" id="inpkode" name="kode_entitas" maxLength="3"  <?=$readonly;?> value="<?=$kode_entitas?>" >
					</div>
				</div>
				<div class="form-group">
					<label for="inpsingkatan" class="col-sm-2 control-label">Singkatan entitas</label>
					<div class="col-sm-1">
						<input type="text" class="form-control" id="inpsingkatan" name="singkatan_entitas" value="<?=$singkatan_entitas?>" maxLength="4" >
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Nama Entitas *</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="inpnama" name="nama_entitas" value="<?=$nama_entitas?>">
					</div>
				</div>
				<div class="form-group">
					<label for="txtdeskripsi" class="col-sm-2 control-label">Deskripsi </label>
					<div class="col-sm-8">
						<textarea class="form-control" id="inpsingkatan" name="deskripsi_entitas" rows="3"><?=$deskripsi_entitas?></textarea>
					</div>
				</div>
				<center><a href="/entitas" class="btn btn-warning">kembali</a> <button class="btn btn-primary">Simpan</button></center>
			</form>
			<p class="text-muted">(*) harus diisi</p>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	
	$("#f1").validate({
		ignore: "",
		rules: {
			kode_entitas: {
				required: true,
				minlength: 3,
				maxlength: 3
			},
			nama_entitas: {
				required: true
			}
		},
		messages: {
			kode_entitas: {
				required: "Masukkan kode entitas!",
				minlength: "kode entitas 3 huruf!",
				maxlength: "kode entitas 3 huruf!"
			},
			nama_entitas: {
				required: "Masukkan nama entitas!"
			}
		}
	});
	
});	
</script>