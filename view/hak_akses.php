<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Hak Akses <small> daftar hak akses.</small></h3>
		<hr class="row-title">
		<div class="row">
			<div class="col-md-4">
				<form class="form-inline" method="POST" action="/hak-akses">
					<div class="input-group">
						<input type="text" class="form-control input-sm" name="keyword" value="<?=$_key?>" placeholder="Search for...">
						<span class="input-group-btn">
							<button class="btn btn-default btn-sm" type="submit"><i class="glyphicon glyphicon-search"></i></button>
						</span>
					</div><!-- /input-group -->
				</form>			
			</div>
			<div class="col-md-4">
			</div>
			<div class="col-md-4">
				<a href="/hak-akses/tambah" class="btn btn-primary  pull-right btn-sm"><i class="glyphicon glyphicon-plus"></i> </a>
			</div>
		</div>
		<br>
		<table class="table table-hover">
			<tr>
				<th>No.</th>
				<th>Kode Hak Akses</th>
				<th>Nama Hak Akses</th>
				<th>&nbsp;</th>
			</tr>
			<?php 
				$no = $start;
				foreach($rs_data AS $data){
					$no++;
					if(is_array($data)){
						foreach($data AS $key=>$val){
							$key  = strtolower($key);
							if(preg_match("/tanggal|tgl/i",$key) && $val!="")
								$val = date("d/m/Y",strtotime($val));
							
							$$key = trim($val);							
						}
					}
					
					echo "
						<tr class=\"odd gradeX\">
							<td>".$no."</td>
							<td>".$kode_hak."</td>
							<td>".$nama_hak."</td>
							<td class=\"center\">
								<a href=\"/hak-akses/ubah/".$kode_hak."\"><i class=\"glyphicon glyphicon-pencil\"></i></a>
								&nbsp;&nbsp;&nbsp;<a data-toggle=\"modal\" data-target=\"#confirm-delete\"><i class=\"glyphicon glyphicon-remove\"></i></a> 
							</td>
						</tr>
						 ";
				}
				if($no<1){
					echo "
						<tr class=\"odd gradeX\">
							<td colspan=\"4\" class=\"center\">** TIDAK ADA DATA **</td>
						</tr>
						 ";					
				}
			?>			
		</table>
		<?php echo $tpl->paging($count_page,$page,$start,"/hak-akses",($_key=="" ? "" : "/".$_key))?>
	</div>
</div>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <b>Konfirmasi</b>
            </div>
            <div class="modal-body">
                Apakah anda berhasil menghapus data ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>
<script>
$('#confirm-delete').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', '/hak-akses/hapus/<?=$kode_hak?>');
});
</script>