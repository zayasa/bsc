<script type="text/javascript" src="/assets/js/jquery.autocomplete.min.js"></script>
<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Scorecard <small> Form tambah/ubah scorecard.</small></h3>
		<hr class="row-title">
		<div class="row">
			<form class="form-horizontal" name="f1" id="f1" action="/<?=$controller?>/<?=$kode_periode?>/<?=$kode_unit_kerja?>/<?=$_action?>" method="POST">
				<input type="hidden" name="kode_periode_scorecard" value="<?=$kode_periode?>" />
				<input type="hidden" name="kode_pemilik_scorecard" value="<?=$kode_unit_kerja?>" />
				<div class="form-group">
					<label for="inptipe" class="col-sm-2 control-label">Tipe Scorecard *</label>
					<div class="col-sm-3">
						<?=$tpl->selectList("tipe_scorecard","tipe_scorecard","kode_tipe_scorecard","nama_tipe_scorecard",$tipe_scorecard," onChange=\"get_nkode(this.value)\" ")?>
					</div>
				</div>
				<div class="form-group">
					<label for="inpindukscore" class="col-sm-2 control-label">Induk Scorecard </label>
					<div class="col-sm-4">
						<?=$tpl->selectList("kode_induk_scorecard","scorecard","kode_scorecard","nama_scorecard",$kode_induk_scorecard,""," WHERE kode_pemilik_scorecard='".$kode_unit_kerja."' AND kode_periode_scorecard='".$kode_periode."' ")?>
					</div>
				</div>
				<div class="form-group">
					<label for="inpkode" class="col-sm-2 control-label">Kode Scorecard *</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="inpkode" name="kode_scorecard"  value="<?=$kode_scorecard?>">
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Nama Scorecard *</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="inpnama" name="nama_scorecard" value="<?=$nama_scorecard?>">
					</div>
				</div>
				<div class="form-group">
					<label for="txtdesc" class="col-sm-2 control-label">Deskripsi scorecard </label>
					<div class="col-sm-8">
						<textarea class="form-control" id="txtdesc" name="deskripsi_scorecard" rows="3"><?=$deskripsi_scorecard?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="txtformula" class="col-sm-2 control-label">Formula scorecard </label>
					<div class="col-sm-8">
						<textarea class="form-control" id="txtformula" name="formula_scorecard" rows="3"><?=$formula_scorecard?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Tipe Cascade</label>
					<div class="col-sm-3">
						<?=$tpl->selectList("tipe_cascade_scorecard","cascade_scorecard","tipe_cascade_scorecard","nama_cascade_scorecard",$tipe_cascade_scorecard,""," ")?>
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Cascade scorecard</label>
					<div class="col-sm-3">
						<?=$tpl->selectList("kode_cascade_scorecard","scorecard AS a LEFT JOIN unit_kerja AS b on a.kode_pemilik_scorecard=b.kode_unit_kerja ","kode_scorecard","nama_unit_kerja,nama_scorecard",$kode_cascade_scorecard,""," WHERE a.kode_pemilik_scorecard='".$uk_kode_induk_unit_kerja."' AND kode_periode_scorecard='".$kode_periode."' ")?>
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Tipe Polarisasi *</label>
					<div class="col-sm-3">
						<?=$tpl->selectList("tipe_polarisasi_scorecard","polarisasi_scorecard","tipe_polarisasi_scorecard","nama_polarisasi_scorecard",$tipe_polarisasi_scorecard,"","")?>
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Satuan Waktu *</label>
					<div class="col-sm-3">
						<?=$tpl->selectList("satuan_waktu_scorecard","satuan_waktu","kode_satuan_waktu","nama_satuan_waktu",$satuan_waktu_scorecard,"","")?>
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Satuan Nilai *</label>
					<div class="col-sm-3">
						<?=$tpl->selectList("satuan_nilai_scorecard","satuan_nilai","kode_satuan_nilai","nama_satuan_nilai",$satuan_nilai_scorecard,"","")?>
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Label Nilai </label>
					<div class="col-sm-3">
						<?=$tpl->selectList("label_nilai_scorecard","label_nilai","kode_label_nilai","nama_label_nilai",$label_nilai_scorecard,"","")?>
					</div>
				</div>
				<?php 
					/*
				?>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label"></label>
					<div class="col-sm-10">

						<ul class="nav nav-tabs">
							<?php
								$no= 0;
								foreach($array_tipe_penilaian AS $key=>$val){
									if($no==0)
										echo "<li class=\"active\"><a data-toggle=\"tab\" href=\"#".$key."\">".$val."</a></li>";
									else 
										echo "<li><a data-toggle=\"tab\" href=\"#".$key."\">".$val."</a></li>";
										
									$no++;
								}
							?>
						</ul>

						<div class="tab-content">
							<?php
								$no= 0;
								foreach($array_tipe_penilaian AS $key=>$val){
									$key = trim($key);
									$val = trim($val);
									if($no=="0")
										echo "<div id=\"".$key."\" class=\"tab-pane fade in active\">";
									else 
										echo "<div id=\"".$key."\" class=\"tab-pane\">";
									
									echo "<h3>".$val."</h3>";
									
									$script .= "ubah_tipe('".$key."','".$array_nilai[$key]['tipe_input_scorecard']."');";
							?>									
							<div class="form-group">
								<input type="hidden" class="form-control autocomplete" name="tipe_nilai_scorecard[<?=$key?>]" value="<?=$key?>">
								<label for="inpnama" class="col-sm-2 control-label">Tipe</label>
								<div class="col-sm-3">
									<label class="radio-inline">
										<input type="radio" onClick="ubah_tipe('<?=$key?>','1')" name="tipe_input_scorecard[<?=$key?>]"  <?=($array_nilai[$key]['tipe_input_scorecard']=="1" ? "checked" : "")?> value="1"> Nilai
									</label>
									<label class="radio-inline">
										<input type="radio" onClick="ubah_tipe('<?=$key?>','2')" name="tipe_input_scorecard[<?=$key?>]" <?=($array_nilai[$key]['tipe_input_scorecard']=="2" ? "checked" : "")?> value="2"> Formula
									</label>								
								</div>
							</div>
							<div id="<?=$key?>_formula_container" style="display:none;" class="form-group">
								<label for="txtdesc" class="col-sm-2 control-label">Formula </label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="text" class="form-control autocomplete" id="<?=$key?>_">
										<span class="input-group-btn">
											<button class="btn btn-default " type="submit"><i class="glyphicon glyphicon-search"></i></button>
										</span>
									</div><!-- /input-group -->									
									<textarea class="form-control" name="formula_nilai_scorecard[<?=$key?>]" id="<?=$key?>_formula_nilai_scorecard" rows="3"><?=$array_nilai[$key]['formula_nilai_scorecard']?></textarea>
								</div>
							</div>
							<div id="<?=$key?>_nilai_container"  class="form-group">
								<label class="col-sm-2 control-label">Nilai </label>
								<div class="col-sm-2">
									<input type="text" class="form-control"  name="nilai_scorecard[<?=$key?>]" id="<?=$key?>_nilai_scorecard"  value="<?=$array_nilai[$key]['nilai_scorecard']?>" >
								</div>
							</div>
							<?php
									echo "</div>";
									$no++;
								}
							?>
						</div>					
					
					</div>
				</div>
				<?php 
					*/
				?>
				<center><a href="/<?=$controller?>/<?=$kode_periode?>" class="btn btn-warning">kembali</a> <button class="btn btn-primary">Simpan</button></center>
			</form>
			<p class="text-muted">(*) harus diisi</p>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	
	$("#f1").validate({
		ignore: "",
		rules: {
			tipe_scorecard: {
				required: true
			},
			kode_scorecard: {
				required: true,
				minlength: 10,
				maxlength: 10
			},
			nama_scorecard: {
				required: true
			},
			tipe_polarisasi_scorecard: {
				required: true
			},
			satuan_waktu_scorecard: {
				required: true
			},
			satuan_nilai_scorecard: {
				required: true
			}
		},
		messages: {
			tipe_scorecard: {
				required: "Pilih tipe scorecard!"
			},
			kode_scorecard: {
				required: "Masukkan kode scorecard!",
				minlength: "kode scorecard 10 huruf!",
				maxlength: "kode scorecard 10 huruf!"
			},
			nama_scorecard: {
				required: "Masukkan nama scorecard!"
			},
			tipe_polarisasi_scorecard: {
				required: "Pilih tipe polarisasi!"
			},
			satuan_waktu_scorecard: {
				required: "Pilih satuan waktu!"
			},
			satuan_nilai_scorecard: {
				required: "Pilih satuan nilai!"
			}
		}
	});
	
});	
function get_nkode(param){
	$.ajax({
		method: "GET",
		url: "/ajax/get_nkode_scorecard.php",
		data: {tipe_scorecard : param}
	})
	.done(function( data ) {
		$( "#inpkode" ).val( data );
	});
}


function ubah_tipe(elem,param){
	if(param=="1"){
		$("#"+elem+"_formula_container").hide();
		$("#"+elem+"_nilai_container").show();
		$("#"+elem+"_nilai_scorecard").prop('readonly',false);
	}
	else{
		$("#"+elem+"_formula_container").show();
		//$("#"+elem+"_nilai_container").hide();
		$("#"+elem+"_nilai_scorecard").prop('readonly',true);
	}
}
var jsonStr = $.parseJSON($.ajax({
	type: "GET",
	url: "/ajax/get_scorecard_formula.php",
	data: {kode_unit_kerja : "<?=$kode_unit_kerja?>",kode_periode : "<?=$kode_periode?>"},
	contentType: "application/json; charset=utf-8",
	dataType: "json",
	async: false, 
	success: function (data) { 
	// nothing needed here 
	}
}) .responseText) ;

$('.autocomplete').autocomplete({
    lookup: jsonStr,
    onSelect: function (suggestion) {
		var ids = $(this).attr("id");
		var old_val =$("#"+ids+"formula_nilai_scorecard").val();
		$("#"+ids+"formula_nilai_scorecard").val(old_val+suggestion.data);
		$(this).val("");
	}
});
<?=$script;?>
</script>
<style>
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; width:inherit !important}
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
</style>
