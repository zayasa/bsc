<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Periode <small> daftar periode scorecard.</small></h3>
		<hr class="row-title">
		<div class="row">
			<form class="form-horizontal" name="f1" id="f1" action="/periode-scorecard/<?=$_action?>" method="POST">
				<div class="form-group">
					<label for="inpkode" class="col-sm-2 control-label">Kode Periode *</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="inpkode" name="kode_periode" maxLength="6"  <?=$readonly;?> value="<?=$kode_periode?>" >
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Nama Periode *</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="inpnama" name="nama_periode" value="<?=$nama_periode?>">
					</div>
				</div>
				<div class="form-group">
					<label for="txtdeskripsi" class="col-sm-2 control-label">Deskripsi </label>
					<div class="col-sm-8">
						<textarea class="form-control" id="txtdeskripsi" name="deskripsi_periode" rows="3"><?=$deskripsi_periode?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="inptanggalmulai" class="col-sm-2 control-label">Tanggal mulai *</label>
					<div class="col-sm-2">
						<input type="date" class="form-control" id="inptanggalmulai" name="tanggal_mulai_periode" value="<?=$tanggal_mulai_periode?>">
					</div>
				</div>
				<div class="form-group">
					<label for="inptanggalselesai" class="col-sm-2 control-label">Tanggal Selesai </label>
					<div class="col-sm-2">
						<input type="date" class="form-control" id="inptanggalselesai" name="tanggal_selesai_periode" value="<?=$tanggal_selesai_periode?>">
					</div>
				</div>
				<center><a href="/periode-scorecard" class="btn btn-warning">kembali</a> <button class="btn btn-primary">Simpan</button></center>
			</form>
			<p class="text-muted">(*) harus diisi</p>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	
	$("#f1").validate({
		ignore: "",
		rules: {
			kode_periode: {
				required: true,
				minlength: 6,
				maxlength: 6
			},
			nama_periode: {
				required: true
			},
			tanggal_mulai_periode: {
				required: true
			}
		},
		messages: {
			kode_periode: {
				required: "Masukkan kode periode!",
				minlength: "kode periode 6 huruf!",
				maxlength: "kode periode 6 huruf!"
			},
			nama_periode: {
				required: "Masukkan nama periode!"
			},
			tanggal_mulai_periode: {
				required: "Masukkan tanggal mulai periode!"
			}
		}
	});
	
});	
</script>