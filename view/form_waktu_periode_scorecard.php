<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Periode <small> daftar waktu periode scorecard.</small></h3>
		<hr class="row-title">
		<div class="row">
			<form class="form-horizontal" >
				<div class="form-group">
					<label for="inpkode" class="col-sm-2 control-label">Kode Periode</label>
					<div class="col-sm-10">
						<p class="form-control-static"><?=$kode_periode?></p>
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Nama Periode</label>
					<div class="col-sm-10">
						<p class="form-control-static"><?=$nama_periode?></p>
					</div>
				</div>
				<div class="form-group">
					<label for="inptanggalmulai" class="col-sm-2 control-label">Tanggal mulai</label>
					<div class="col-sm-10">
						<p class="form-control-static"><?=$tanggal_mulai_periode?> s/d <?=$tanggal_selesai_periode?></p>
					</div>
				</div>
			</form>
		</div>
		<hr class="row-title">
		<div class="row">
			<form class="form-horizontal" >
				<div class="form-group">
					<label for="inpkode" class="col-sm-2 control-label">Kode Satuan waktu</label>
					<div class="col-sm-10">
						<p class="form-control-static"><?=$kode_satuan_waktu?></p>
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Satuan waktu</label>
					<div class="col-sm-10">
						<p class="form-control-static"><?=$nama_satuan_waktu?></p>
					</div>
				</div>
			</form>
		</div>
		<hr class="row-title">
		<div>
			<form class="form-horizontal" name="f1" id="f1" action="/waktu-periode-scorecard/<?=$kode_periode?>/<?=$kode_satuan_waktu?>/atur" method="POST">
				<div class="form-group">
					<label for="inpkode" class="col-sm-2 control-label">Periode </label>
					<div class="col-sm-10">
						<table class="table table-bordered">
							<tr>
								<th>Tanggal Mulai </th>
								<th>Tanggal Selesai </th>
								<th>Batas pengisian target</th>
								<th>Tanggal Penutupan</th>
							</tr>
							<?php 
								if($rs_total>=1){
									foreach($rs_data AS $data){
										foreach($data AS $ky=>$vl){
											$ky  = strtolower($ky);
											$$ky = trim($vl);
										}
										echo "
											<tr>
												<th><input type=\"date\" class=\"form-control\" id=\"inptanggalselesai\" name=\"tanggal_mulai[]\" value=\"".$tanggal_mulai."\"></th>
												<th><input type=\"date\" class=\"form-control\" id=\"inptanggalselesai\" name=\"tanggal_selesai[]\" value=\"".$tanggal_selesai."\"></th>
												<th><input type=\"date\" class=\"form-control\" id=\"inptanggalselesai\" name=\"tanggal_batas_target[]\" value=\"".$tanggal_batas_target."\"></th>
												<th><input type=\"date\" class=\"form-control\" id=\"inptanggalselesai\" name=\"tanggal_tutup_periode[]\" value=\"".$tanggal_tutup_periode."\"></th>
											</tr>
											 ";										
									}
									
								}
								else{
									$date = new DateTime();
									list($_day,$_month,$_year) = explode("/",$tanggal_mulai_periode);
									$date->setDate($_year, (int)$_month, (int)$_day);
									$tanggal_mulai_periode = $date->format('Y-m-d');
									list($day,$month,$year) = explode("/",$tanggal_selesai_periode);
									$date->setDate($year, (int)$month, (int)$day);
									$tanggal_selesai_periode = $date->format('Y-m-d');
									while($tanggal_mulai_periode <= $tanggal_selesai_periode){
										$start = date("Y-m-d",strtotime($tanggal_mulai_periode));
										$end = date("Y-m-d",strtotime("".$formula_satuan_waktu." -1 Day",strtotime($tanggal_mulai_periode)));
										$temp_end = date("Y-m-d",strtotime("".$formula_satuan_waktu."",strtotime($tanggal_mulai_periode)));
										
										if($temp_end>$tanggal_selesai_periode)
											$end = date("Y-m-d",strtotime($tanggal_selesai_periode));
										
										$batas_target = date("Y-m-d",strtotime("+10 Day",strtotime($start)));
										$batas_akumulasi = date("Y-m-d",strtotime("+10 Day",strtotime($end)));
										echo "
											<tr>
												<th><input type=\"date\" class=\"form-control\" id=\"inptanggalselesai\" name=\"tanggal_mulai[]\" value=\"".$start."\"></th>
												<th><input type=\"date\" class=\"form-control\" id=\"inptanggalselesai\" name=\"tanggal_selesai[]\" value=\"".$end."\"></th>
												<th><input type=\"date\" class=\"form-control\" id=\"inptanggalselesai\" name=\"tanggal_batas_target[]\" value=\"".$batas_target."\"></th>
												<th><input type=\"date\" class=\"form-control\" id=\"inptanggalselesai\" name=\"tanggal_tutup_periode[]\" value=\"".$batas_akumulasi."\"></th>
											</tr>
											 ";
										$tanggal_mulai_periode = date("Y-m-d",strtotime("".$formula_satuan_waktu."",strtotime($tanggal_mulai_periode)));
									}
									
								}
							?>
						</table>
					</div>
				</div>
				<center><a href="/waktu-periode-scorecard/<?=$kode_periode?>/<?=$kode_satuan_waktu?>" class="btn btn-warning">kembali</a> <button class="btn btn-primary">Simpan</button></center>
			</form>
		</div>
		<br>
</div>