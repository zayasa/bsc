<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Pegawai <small> daftar pegawai.</small></h3>
		<hr class="row-title">
		<div class="row">
			<div class="col-md-4">
				<form class="form-inline" method="POST" action="/scorecard-pegawai/<?=$kode_periode?>">
					<div class="input-group">
						<input type="text" class="form-control input-sm" name="keyword" value="<?=$_key?>" placeholder="Search for...">
						<span class="input-group-btn">
							<button class="btn btn-default btn-sm" type="submit"><i class="glyphicon glyphicon-search"></i></button>
						</span>
					</div><!-- /input-group -->
				</form>			
			</div>
			<div class="col-md-4">
			</div>
		</div>
		<br>
		<table class="table table-hover">
			<tr>
				<th>No.</th>
				<th>Kode Pegawai</th>
				<th>Nama Pegawai</th>
				<th>Entitas</th>
				<th>Unit Kerja</th>
				<th>Jabatan</th>
				<th>Status</th>
				<th>&nbsp;</th>
			</tr>
			<?php 
				$no = $start;
				foreach($rs_data AS $data){
					$no++;
					if(is_array($data)){
						foreach($data AS $key=>$val){
							$key  = strtolower($key);
							if(preg_match("/tanggal|tgl/i",$key) && $val!="")
								$val = date("d/m/Y",strtotime($val));
							
							$$key = trim($val);							
						}
					}
					
					if((!empty($tanggal_keluar_pengguna) && $tanggal_keluar_pengguna<=date("d/m/Y")) || $status_pengguna=="0"){
						$class = "btn-danger";
						$status = "non-aktif";
					}
					else if($status_pengguna=="2"){
						$class = "btn-warning";
						$status = "ditangguhkan";
					}
					else{
						$class = "btn-success";
						$status = "aktif";
					}
					
					echo "
						<tr class=\"odd gradeX\">
							<td>".$no."</td>
							<td>".$kode_pengguna."</td>
							<td>".$nama_pengguna."</td>
							<td>".$nama_entitas."</td>
							<td>".$nama_unit_kerja."</td>
							<td>".$nama_jabatan."</td>
							<td><button class=\"btn ".$class." btn-indicator\" title=\"".$status."\">&nbsp;</button></td>
							<td class=\"center\">
								<a href=\"/scorecard-pegawai/".$kode_periode."/".$kode_pengguna."\"><i class=\"glyphicon glyphicon-list-alt\"></i></a>
							</td>
						</tr>
						 ";
				}
				if($no<1){
					echo "
						<tr class=\"odd gradeX\">
							<td colspan=\"9\" class=\"center\">** TIDAK ADA DATA **</td>
						</tr>
						 ";					
				}
			?>			
		</table>
		<?php //echo $tpl->paging($count_page,$page,$start,"/scorecard-pegawai/".$kode_periode,($_key=="" ? "" : "/".$_key))?>
	</div>
</div>