<?php
$header_page = "";
$footer_page = "";
if($keluaran=="excel"){
	$contents = ob_get_contents();
	ob_end_clean();
	
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=laporan pegawai.xls");	
}
else{
	$header_page = "
<div class=\"panel panel-default borderless\">
	<div class=\"panel-body\">
				   ";
	$footer_page = "		
	</div>
</div>
				   ";
}
echo $header_page;
?>
		<table class="table table-hover">
			<tr>
				<th>No.</th>
				<th>Kode Pengguna</th>
				<th>Nama Pengguna</th>
				<th>Tanggal lahir</th>
				<th>Alamat</th>
				<th>Telpon</th>
				<th>Email</th>
				<th>Entitas</th>
				<th>Unit Kerja</th>
				<th>Jabatan</th>
				<th>Tanggal Masuk</th>
				<th>Tanggal Keluar</th>
			</tr>
			<?php 
				$no = 0;
				foreach($rs_data AS $data){
					$no++;
					if(is_array($data)){
						foreach($data AS $key=>$val){
							$key  = strtolower($key);
							if(preg_match("/tanggal|tgl/i",$key) && $val!="")
								$val = date("d/m/Y",strtotime($val));
							
							$$key = trim($val);							
						}
					}
					
					
					echo "
						<tr class=\"odd gradeX\">
							<td>".$no."</td>
							<td>".$kode_pengguna."</td>
							<td>".$nama_pengguna."</td>
							<td>".$tanggal_lahir_pengguna."</td>
							<td>".$alamat_pengguna."</td>
							<td>".$telpon_pengguna."</td>
							<td>".$email_pengguna."</td>
							<td>".$nama_entitas."</td>
							<td>".$nama_unit_kerja."</td>
							<td>".$nama_jabatan."</td>
							<td>".$tanggal_masuk_pengguna."</td>
							<td>".$tanggal_keluar_pengguna."</td>
						</tr>
						 ";
				}
				if($no<1){
					echo "
						<tr class=\"odd gradeX\">
							<td colspan=\"9\" class=\"center\">** TIDAK ADA DATA **</td>
						</tr>
						 ";					
				}
			?>			
		</table>
<?=$footer_page?>