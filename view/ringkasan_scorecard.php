<link rel='StyleSheet' href='/assets/component/morris.js/morris.css' type='text/css' />
<script type="text/javascript" src="/assets/component/morris.js/raphael.js"></script>
<script type="text/javascript" src="/assets/component/morris.js/morris.min.js"></script>

<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Ringkasan scorecard</h3>
		<hr class="row-title">
		<div class="row">
			<form class="form-horizontal" name="f1" id="f1" action="/ringkasan-scorecard/<?=$kode_periode?>" method="POST">
				<div class="form-group">
					<label for="inpkode" class="col-sm-2 control-label text-left">Periode</label>
					<div class="col-sm-3">
						<?=$tpl->selectList("kode_periode","periode","kode_periode","nama_periode",$kode_periode)?>
					</div>
					<div class="col-sm-3">
						<button type="submit" class="btn btn-default">lihat</button>
					</div>
				</div>
			</form>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div id="dashboard-satu" style="height: 300px;"></div>
			</div>
			<div class="col-md-6">
				<table class="table table-hover">
					<tr>
						<td><b>No. </b></td>
						<td><b>Pemilik Scorecard</b></td>
						<td><b>Target</b></td>
						<td><b>Realisasi</b></td>
						<td></td>
					</tr>
					<?php 
						$no =0;
						$jsonn = "";
						foreach($array_unit AS $key=>$val){
							$no++;
							$target = ($array_dash1[$key]['TAR']=="" ? 0 : $array_dash1[$key]['TAR']);
							$score = ($array_dash1[$key]['SCO']=="" ? 0 : $array_dash1[$key]['SCO']);
							
							if($level_unit_kerjaa<5)
								$button = "<a href=\"/".$controller."/".$kode_periode."/".$level_unit_kerjaa."/".$key."\" type=\"button\" class=\"btn btn-default btn-xs\"><span class=\"glyphicon glyphicon-arrow-down\" aria-hidden=\"true\"></span></a> ";
							else 
								$button = "";
							echo "<tr><td>".$no."</td><td>".$val."</td><td>".$target."</td><td>".$score."</td><td>".$button." <button class=\"btn btn-default btn-xs\" onClick=\"getPerspektif('".$kode_periode."','".$key."')\"><span class=\"glyphicon glyphicon-search\" aria-hidden=\"true\"></span></button></td></tr>";
							$jsonn .=  "{ y: '".$val."', a: '".$target."', b: '".$score."' },";
						}
					?>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="dashboard-dua" style="height: 300px;"></div>
			</div>
		</div>
	</div>
</div>
<script>
new Morris.Bar({
  element: 'dashboard-satu',
  data: [
  <?=$jsonn?>
  ],
  xkey: 'y',
  ykeys: ['a', 'b'],
  labels: ['Target', 'Realisasi']
  });

function getPerspektif(k_periode,k_unit_kerja){
	$.ajax({
		method: "GET",
		url: "/ajax/get_perspektif_scorecard.php",
		data: {kode_unit_kerja : k_unit_kerja,kode_periode : k_periode },
		cache: false,
		async: false
	})
	.done(function( dataa ) {
		$("#dashboard-dua").html("");
		new Morris.Bar({
			element: 'dashboard-dua',
			data: JSON.parse(decodeURIComponent(dataa)) ,
			xkey: 'y',
			ykeys: ['a', 'b'],
			labels: ['Target', 'Skor']
		});
	});
}
</script>
