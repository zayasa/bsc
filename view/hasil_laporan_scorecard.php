<?php
$header_page = "";
$footer_page = "";
if($keluaran=="excel"){
	$contents = ob_get_contents();
	ob_end_clean();
	
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=laporan scorecard.xls");	
}
else{
	$header_page = "
<div class=\"panel panel-default borderless\">
	<div class=\"panel-body\">
				   ";
	$footer_page = "		
	</div>
</div>
				   ";
}
echo $header_page;
?>
		<table class="table table-hover">
			<tr>
				<th>No.</th>
				<th>Scorecard</th>
				<th>Target</th>
				<th>Realisasi</th>
				<th>Skor</th>
			</tr>
			<?php 
				$no = 0;
				foreach($array_scorecard AS $key=>$data){
					
					$no++;
					foreach($data AS $k=>$v){
						$k  = strtolower($k);
						$$k = trim($v);
					}
					
					$c_deepline = count(explode(",",$urutan));
					
					$deepline = "";
					for($a=1;$a<=$c_deepline;$a++){
						$deepline .= "&nbsp;&nbsp;&nbsp;";
					}
					echo  "
							<tr class=\"odd gradeX\">
								<td>".$no."</td>
								<td>".$deepline.$nama_scorecard."</td>
								<td>".$target."</td>
								<td>".$realisasi."</td>
								<td>".$skor."</td>
							</tr>
						  ";
				}
				if($no<1){
					echo "
						<tr class=\"odd gradeX\">
							<td colspan=\"9\" class=\"center\">** TIDAK ADA DATA **</td>
						</tr>
						 ";					
				}
			?>			
		</table>
<?=$footer_page?>
