<script type="text/javascript" src="/assets/js/jquery.autocomplete.min.js"></script>


<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Scorecard <small> daftar nilai scorecard.</small></h3>
		<hr class="row-title">
		<div class="row">
			<form class="form-horizontal" name="f1" id="f1" action="/<?=$controller?>/<?=$kode_periode?>/<?=$kode_unit_kerja?>//<?=$kode_scorecard?>" method="POST">
				<input type="hidden" name="kode_periode_scorecard" value="<?=$kode_periode?>" />
				<input type="hidden" name="kode_pemilik_scorecard" value="<?=$kode_unit_kerja?>" />
				<div class="form-group">
					<label for="inptipe" class="col-sm-2 control-label">Tipe Scorecard</label>
					<div class="col-sm-3">
						<p class="form-control-static"><?=$nama_tipe_scorecard?></p>
					</div>
				</div>
				<div class="form-group">
					<label for="inpkode" class="col-sm-2 control-label">Kode Scorecard</label>
					<div class="col-sm-2">
						<p class="form-control-static"><?=$kode_scorecard?></p>
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Nama Scorecard</label>
					<div class="col-sm-8">
						<p class="form-control-static"><?=$nama_scorecard?></p>
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Tipe Polarisasi</label>
					<div class="col-sm-3">
						<p class="form-control-static"><?=$nama_polarisasi_scorecard?></p>
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Satuan Waktu</label>
					<div class="col-sm-3">
						<p class="form-control-static"><?=$nama_satuan_waktu?></p>
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Satuan Nilai</label>
					<div class="col-sm-3">
						<p class="form-control-static"><?=$nama_satuan_nilai?></p>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<table class="table table-bordered">
							<tr>
								<th>periode </th>
								<th>Target</th>
								<th>Standar</th>
								<th>Min</th>
								<th>Max</th>
								<th>Realisasi</th>
								<th>Score</th>
								<th>&nbsp;</th>
							</tr>
							<?php 
								foreach($rs_periode AS $periode){
									foreach($periode AS $k=>$v){
										$k  = strtolower($k);										
										if(preg_match("/tanggal|tgl/i",$k))
											$v = date("d/m/Y",strtotime($v));
										
										if(preg_match("/_tar|_bse|_max|_min|_ral|_sco/i",$k)){										
											$prefix = "";
											$sufix = "";
											/**/
											if($satuan_nilai_scorecard=="num" && $v!=""){
												if(is_decimal($v)===true)
													$v = number_format($v, 2, ',', '.');
												else 
													$v = number_format($v, 0, ',', '.');
											}
											else if($satuan_nilai_scorecard=="tgll"  && $v!=""){
												$v = date("d/m/Y",strtotime($v));
											}
											
											if($arr_label_nilai['penempatan_label']=="1")
												$prefix = $arr_label_nilai['lambang_label_nilai']." ";
											
											if($arr_label_nilai['penempatan_label']=="2")
												$sufix = " ".$arr_label_nilai['lambang_label_nilai'];
										
											if(!preg_match("/_sco/i",$k))
												$v = $prefix.trim($v).$sufix;
											else 
												$v = trim($v);
										}
										
										$$k = trim($v);
									}
									
										
								echo "<tr>
										<td>".$tanggal_mulai." s/d ".$tanggal_selesai."</td>
										<td>".$_tar."</td>
										<td>".$_bse."</td>
										<td>".$_min."</td>
										<td>".$_max."</td>
										<td>".$_ral."</td>
										<td>".$_sco."</td>
										<td class=\"center\">
											<a href=\"/".$controller."/".$kode_periode."/".$kode_unit_kerja."/nilai/".$kode_scorecard."/".$id."\" ><i class=\"glyphicon glyphicon-pencil\"></i></a>
										</td>
									  </tr>";
								}
							?>
						</table>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>