<div class="panel panel-default borderless">
	<div class="panel-body">
		<h3 class="page-title">Pengguna <small> Form tambah/ubah pengguna .</small></h3>
		<hr class="row-title">
		<div class="row">
			<form class="form-horizontal" name="f1" id="f1" action="/pengguna/<?=$_action?>" method="POST">
				<div class="form-group">
					<label for="inpkode" class="col-sm-2 control-label">Kode Pengguna *</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="inpkode" name="kode_pengguna" maxLength="10"  <?=$readonly;?> value="<?=$kode_pengguna?>" >
					</div>
				</div>
				<div class="form-group">
					<label for="inpnama" class="col-sm-2 control-label">Nama Pengguna *</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="inpnama" name="nama_pengguna" value="<?=$nama_pengguna?>">
					</div>
				</div>
				<div class="form-group">
					<label for="inptanggallahir" class="col-sm-2 control-label">Tanggal lahir *</label>
					<div class="col-sm-2">
						<input type="date" class="form-control" id="inptanggallahir" name="tanggal_lahir_pengguna" value="<?=$tanggal_lahir_pengguna?>">
					</div>
				</div>
				<div class="form-group">
					<label for="txtalamat" class="col-sm-2 control-label">Alamat </label>
					<div class="col-sm-8">
						<textarea class="form-control" id="txtalamat" name="alamat_pengguna" rows="3"><?=$alamat_pengguna?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="inptelpon" class="col-sm-2 control-label">Telpon </label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="inptelpon" name="telpon_pengguna" value="<?=$telpon_pengguna?>">
					</div>
				</div>
				<div class="form-group">
					<label for="inpemail" class="col-sm-2 control-label">Email </label>
					<div class="col-sm-4">
						<input type="email" class="form-control" id="inpemail" name="email_pengguna" value="<?=$email_pengguna?>">
					</div>
				</div>
				<hr class="row-title">
				<div class="form-group">
					<label for="inpentitas" class="col-sm-2 control-label">Entitas *</label>
					<div class="col-sm-4">
						<?=$tpl->selectList("kode_entitas_pengguna","entitas","kode_entitas","nama_entitas",$kode_entitas_pengguna)?>
					</div>
				</div>
				<div class="form-group">
					<label for="inpunitkerja" class="col-sm-2 control-label">Unit Kerja *</label>
					<div class="col-sm-4">
						<?=$tpl->selectList("kode_unit_kerja_pengguna","unit_kerja","kode_unit_kerja","nama_unit_kerja",$kode_unit_kerja_pengguna,"")?>
					</div>
				</div>
				<div class="form-group">
					<label for="inpjabatan" class="col-sm-2 control-label">Jabatan *</label>
					<div class="col-sm-4">
						<?=$tpl->selectList("kode_jabatan_pengguna","jabatan","kode_jabatan","nama_jabatan",$kode_jabatan_pengguna,"")?>
					</div>
				</div>
				<div class="form-group">
					<label for="inptanggalmulai" class="col-sm-2 control-label">Tanggal Masuk *</label>
					<div class="col-sm-2">
						<input type="date" class="form-control" id="inptanggalmulai" name="tanggal_masuk_pengguna" value="<?=$tanggal_masuk_pengguna?>">
					</div>
				</div>
				<div class="form-group">
					<label for="inptanggalselesai" class="col-sm-2 control-label">Tanggal Keluar </label>
					<div class="col-sm-2">
						<input type="date" class="form-control" id="inptanggalselesai" name="tanggal_keluar_pengguna" value="<?=$tanggal_keluar_pengguna?>">
					</div>
				</div>
				<hr class="row-title">
				<div class="form-group">
					<label for="inpjabatan" class="col-sm-2 control-label">Hak akses *</label>
					<div class="col-sm-4">
						<?=$tpl->selectList("kode_hak_akses_pengguna","hak_akses","kode_hak","nama_hak",$kode_hak_akses_pengguna,"")?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Status *</label>
					<div class="col-sm-2">
						<select class="form-control" name="status_pengguna" id="inplevel">
						<option></option>
						<?php
							$status_pengguna = ($status_pengguna!="" ? $status_pengguna : "1");
							foreach($list_status_pengguna AS $key=>$val){
								echo "<option value=\"".$key."\" ".($key==$status_pengguna ? "selected " : "").">".$val."</option>";
							}
						?>
						</select>
					</div>
				</div>				
				<center><a href="/pengguna" class="btn btn-warning">kembali</a> <button class="btn btn-primary">Simpan</button></center>
			</form>
			<p class="text-muted">(*) harus diisi</p>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	
	$("#f1").validate({
		ignore: "",
		rules: {
			kode_pengguna: {
				required: true,
				minlength: 8,
				maxlength: 8
			},
			nama_pengguna: {
				required: true
			},
			tanggal_lahir_pengguna: {
				required: true
			},
			kode_entitas_pengguna: {
				required: true
			},
			kode_unit_kerja_pengguna: {
				required: true
			},
			kode_jabatan_pengguna: {
				required: true
			},
			tanggal_masuk_pengguna: {
				required: true
			},
			kode_hak_akses_pengguna: {
				required: true
			}
		},
		messages: {
			kode_pengguna: {
				required: "Masukkan kode pengguna!",
				minlength: "kode pengguna 8 huruf!",
				maxlength: "kode pengguna 8 huruf!"
			},
			nama_pengguna: {
				required: "Masukkan nama!"
			},
			tanggal_lahir_pengguna: {
				required: "Masukkan tanggal lahir!"
			},
			kode_entitas_pengguna: {
				required: "Pilih entitas!"
			},
			kode_unit_kerja_pengguna: {
				required: "Pilih unit kerja!"
			},
			kode_jabatan_pengguna: {
				required: "Pilih unit kerja!"
			},
			tanggal_masuk_pengguna: {
				required: "Masukkan tanggal masuk!"
			},
			kode_hak_akses_pengguna: {
				required: "Pilih hak akses!"
			}
		}
	});
	
});	
</script>