<?php

class _global{
		
	var $db;

	function get_last_record($sql){
		global $db;
		$result = $db->SelectLimit($sql,1,0);
		if(!$result) {
			print $db->ErrorMsg();
		}

		$row = $result->FetchRow();
		$array = array();
		if(is_array($row)){
			foreach($row as $key=>$val){
				$key  = strtolower($key);
				$$key = $val;
				$array[$key] = $val;
			}
		}
		return $array;
	}
	
	function convert_value($var){

		global $db,$f;
		$table = $var[table];
		$vd = strtolower($var[vd]);
		$cd = $var[cd];
		$cond = $var["cond"];
		if(!empty($cond)){

		}elseif(eregi(",",$cd)){
			$cond = $this->parsing_sql_cond("$cd");
		}else{
			$cond = "lower($cd) = '$vd' ";
		}
		$sql = "select $var[cs] as x from $table where $cond ";
		if($var[print_query]=='1')echo $sql;
		$result         = $db->Execute($sql);
		if (!$result){
			print $db->ErrorMsg();
		}
		$row = $result->FetchRow();
		$new_value = $row['x'];
		return $new_value;
	}

	function pre($input,$die='0'){
		echo "<pre>";
		print_r($input);
		echo "</pre>";
		
		if($die=='1')
			die();

	}
	
	function count_total($table,$cond="",$debug=""){
		global $db;
		$sql="select count(1) as TOTAL from $table $cond";
		if($debug)$this->pre($sql);
		$result_total=$db->Execute("$sql");
		if(!$result_total){
			print $db->ErrorMsg();
		}
		$row_total=$result_total->FetchRow();
		$total=$row_total[TOTAL];
		return $total;
	}
	
	function box($title,$description,$button="",$type="error",$width=""){

		if($type=='error') $style = "panel-danger";
		if($type=='info') $style = "panel-info";
		if($type=='warning') $style = "panel-warning";

		if(is_array($button)){
			for($i=0;$i<count($button);$i++){
				if($button[$i][0]=='back'){
					$action="javascript:history.back(-1)";
				}elseif(!empty($button[$i][3])){
					$action=$button[$i][3];
				}else{
					$action="location.href='".$button[$i][0]."'";
				}
				$button = "<a onClick=\"$action\" class=\"btn btn-default\">".$button[$i][1]."</a>";
			}
		}
		else{
			$button = "<a onClick=\"history.back(-1);\" class=\"btn btn-default\">Kembali</a>";
		}
	

		echo "
                <div class=\"col-md-6 col-sm-6\" style=\"position:fixed;top: 10%;left: 40%;margin-left: -15em; /*set to a negative number 1/2 of your width*/\">
                    <div class=\"panel ".$style."\">
                        <div class=\"panel-heading\">
                            ".$title."
                        </div>
                        <div class=\"panel-body\">
                            <p>".$description."</p>
                        </div>
                        <div class=\"panel-footer\">
                            ".$button."
                        </div>
                    </div>
                </div>
			 ";
		if($type=='error') die();		
	}
		
	function alert_panel($title,$description,$button="",$type="error",$width=""){

		if($type=='error') $style = "alert-danger";
		if($type=='info') $style = "alert-info";
		if($type=='warning') $style = "alert-warning";

		if(is_array($button)){
			if($button[0]=='back'){
				$action="javascript:history.back(-1)";
			}
			else{
				$action="location.href='".$button[1]."'";
			}
			$button = "<a onClick=\"$action\" >".$button[0]."</a>";
		}
		else{
			$button = "<a onClick=\"history.back(-1);\" >Kembali</a>";
		}
	

		echo "
				<div class=\"alert ".$style."\" role=\"alert\">
					<b>".$title."</b>
					<hr class=\"row-title\">
					".$description."<br>
					".$button."
				</div>
			 ";
		if($type=='error') die();		
	}
		
	function redirect($delay="2",$link = "",$message = ""){
		if(empty($link)) $link = "/controller/logout.php";

		echo"<html><head>
		      <META HTTP-EQUIV=\"Refresh\" Content = \"".$delay."; URL=".$link."\">
		      </head><body bgcolor=#FFFFFF><font face=verdana size=2 color=#000000>".$message."
			</body></html>";

	}

	function convert_date_sql($tanggal,$format="1"){
		$tanggal_array=split("\/",$tanggal); //format 31/01/2013
		$_tanggal_array=count($tanggal_array);
		if( $_tanggal_array > 1){
			$output="".$tanggal_array[2]."-".$tanggal_array[1]."-".$tanggal_array[0]."";
		}
		else{
			$output="NULL";
		}
		return $output;	
	}

	function ribuan($angka){
		if(empty($angka)) $angka=0;
		$angka = number_format($angka,"0",",",".");
		return $angka;
	}
	function terbilang($bilangan) {

		$angka = array('0','0','0','0','0','0','0','0','0','0',
		'0','0','0','0','0','0');
		$kata = array('','satu','dua','tiga','empat','lima',
		'enam','tujuh','delapan','sembilan');
		$tingkat = array('','ribu','juta','milyar','triliun');

		$panjang_bilangan = strlen($bilangan);

		/* pengujian panjang bilangan */
		if ($panjang_bilangan > 15) {
			$kalimat = "Diluar Batas";
			return $kalimat;
		}

		/* mengambil angka-angka yang ada dalam bilangan,
		dimasukkan ke dalam array */
		for ($i = 1; $i <= $panjang_bilangan; $i++) {
			$angka[$i] = substr($bilangan,-($i),1);
		}

		$i = 1;
		$j = 0;
		$kalimat = "";


		/* mulai proses iterasi terhadap array angka */
		while ($i <= $panjang_bilangan) {

			$subkalimat = "";
			$kata1 = "";
			$kata2 = "";
			$kata3 = "";

			/* untuk ratusan */
			if ($angka[$i+2] != "0") {
				if ($angka[$i+2] == "1") {
					$kata1 = "Seratus";
				} else {
					$kata1 = $kata[$angka[$i+2]] . " ratus";
				}
			}

			/* untuk puluhan atau belasan */
			if ($angka[$i+1] != "0") {
				if ($angka[$i+1] == "1") {
					if ($angka[$i] == "0") {
						$kata2 = "Sepuluh";
					} elseif ($angka[$i] == "1") {
						$kata2 = "Sebelas";
					} else {
						$kata2 = $kata[$angka[$i]] . " belas";
					}
				} else {
					$kata2 = $kata[$angka[$i+1]] . " puluh";
				}
			}

			/* untuk satuan */
			if ($angka[$i] != "0") {
				if ($angka[$i+1] != "1") {
					$kata3 = $kata[$angka[$i]];
				}
			}

			/* pengujian angka apakah tidak nol semua,
			lalu ditambahkan tingkat */
			if (($angka[$i] != "0") OR ($angka[$i+1] != "0") OR
			($angka[$i+2] != "0")) {
				$subkalimat = "$kata1 $kata2 $kata3 " . $tingkat[$j] . " ";
			}

			/* gabungkan variabe sub kalimat (untuk satu blok 3 angka)
			ke variabel kalimat */
			$kalimat = $subkalimat . $kalimat;
			$i = $i + 3;
			$j = $j + 1;

		}

		/* mengganti satu ribu jadi seribu jika diperlukan */
		if (($angka[5] == "0") AND ($angka[6] == "0")) {
			$kalimat = str_replace("satu ribu","Seribu",$kalimat);
		}

		return trim($kalimat);

	}
	
	function selectList($name,$table,$option_name,$value_name,$curr_id,$script="",$cond="",$sql="",$comparison="equal") {
		global $db;
		$output		 = "<select class=\"form-control\" name=\"".$name."\" $script id=\"".$name."\" >\n";
		$output		.= "<option></option>\n";
		if($sql==""){
			$sql="select * from $table $cond ";
			if (!preg_match("/order/i", $sql)) $sql = $sql ." order by $value_name ";
		}

		$result = $db->Execute("$sql");
		if (!$result){
			print $db->ErrorMsg();
		}
		while ( $row = $result->FetchRow() ) {
			foreach($row as $key=>$val){
				$key=strtolower($key);
				$$key=trim($val);
			}
			if(preg_match("#\smultiple\s#i",$script)){
				if(is_array($curr_id)){
					$selected=in_array($$option_name,$curr_id)?"selected":"";
				}
			}
			else{
				if ($comparison=='equal') 
					$selected=(trim($curr_id)==trim($$option_name))?"selected":"";
				else if ($comparison=='identical') 
					$selected=(trim($curr_id)===trim($$option_name))?"selected":"";					
			}
			
			$output .= "<option value='".$$option_name."' $selected>";
			if(eregi(",",$value_name)){
				$value_name_arr=split(",",$value_name);
				unset($_output);
				for($i=0;$i< count($value_name_arr);$i++){
					$key_val = $value_name_arr[$i];
					$val = $$value_name_arr[$i];
					if(eregi("tgl|tanggal|mulai|selesai",$key_val))
						$val = date("d/m/Y",strtotime($val));
					
					if($val!="01/01/1970")
						$_output .=" ".$val." |";

				}
				$output .=preg_replace("/\|$/","",$_output);
			}else{	
				$output.= $$value_name;
			}
			$output .="</option>\n";
		}
		$result->Close();

		$output .= "</select>\n";
		return $output;
	}
	
	function check_exist_value($sql){
		global $db;
		if(empty($sql)) die("Query tidak boleh kosong");
		$result=$db->SelectLimit("$sql",1,0);
		if(!$result) print $db->ErrorMsg();
		$row=$result->FetchRow();
		$array=array();
		if(is_array($row)){
			foreach($row as $key=>$val){
				$key=strtolower($key);
				$array[$key]=$val;
			}
		}

		if(count($array) > 0) {
			return true;
		}else{
			return false;
		}
	}
		
	function get_server_var($name, $default) {
		global $HTTP_SERVER_VARS, $HTTP_ENV_VARS;
		if( !is_string($name) || empty($name) )
		{
			return $default;
		}
		if( isset($_SERVER[$name]) )
		{
			return $_SERVER[$name];
		}
		else if( isset($_ENV[$name]) )
		{
			return $_ENV[$name];
		}
		else if( isset($HTTP_SERVER_VARS[$name]) )
		{
			return $HTTP_SERVER_VARS[$name];
		}
		else if( isset($HTTP_ENV_VARS[$name]) )
		{
			return $HTTP_ENV_VARS[$name];
		}
		else if( @getenv($name) !== false )
		{
			return getenv($name);
		}
		return $default;
	}

	function get_user_ip() {
		global $client_ip;
		if( isset($client_ip) )
		{
			return $client_ip;
		}
		global $HTTP_CLIENT_IP;
		global $HTTP_X_FORWARDED_FOR;
		global $HTTP_FROM;
		global $REMOTE_ADDR;

		$PIP = '([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])';
		$UIP = '';
		$CIP = $this->get_server_var('HTTP_CLIENT_IP', false);
		$FIP = $this->get_server_var('HTTP_X_FORWARDED_FOR', false);
		$MIP = $this->get_server_var('HTTP_FROM', false);
		$RIP = $this->get_server_var('REMOTE_ADDR', false);
		if( $CIP && $RIP )
		{
			$UIP = $CIP;
		}
		else if( $FIP && $RIP )
		{
			$UIP = $FIP;
		}
		else if( $MIP && $RIP )
		{
			$UIP = $MIP;
		}
		else if( $RIP ){
			$UIP = $RIP;
		}
		$client_ip=$UIP;
		if( empty($client_ip) ){
			die("You are online using invalid IP address $RIP $REMOTE_ADDR");
		}
		return $client_ip;
	}

	function check_session(){
		global $_SESSION,$db;
		
		$return_value = false;
		
		
		if(is_array($_SESSION)){
			foreach($_SESSION AS $k=>$v){ 
				$k  = strtolower($k);
				$$k = trim($v);
			}
		}
		if(isset($_SESSION['login_session'])){
			
			$query_check_user = "SELECT * FROM sesi_pengguna WHERE STATUS='1' AND sesi_id='".$login_session."' ";
			if($this->check_exist_value($query_check_user)===true){
				
				$query_detail_user = "SELECT * FROM pengguna WHERE kode_pengguna='".$login_nip."' ";
				if($this->check_exist_value($query_detail_user)===true){
					
					/* detail session */
					$time = explode(" ",microtime());
					$timeNow = (double)$time[1];
					$result = $this->get_last_record($query_check_user);
					foreach($result AS $k1=>$v1){
						$k1  = "du_".strtolower($k1);
						$$k1 = trim($v1);
					}
									
					$return_value = true;				
					
				}
				else{
					$return_value = false;
					$this->redirect("1","/logout","<img src=\"/assets/img/loading.gif\">Invalid Information for your session..");
				}
				
			}
			else{
				
				$return_value = false;
				$this->redirect("1","/logout","<img src=\"/assets/img/loading.gif\">Invalid Information for your session..");
				
			}
		}
		else{
			$return_value = false;
		}
		
		return $return_value;
		
	}
	
	
	function check_access(){
		global $_SESSION,$db;

		if(empty($_SESSION["login_nip"])) {
			$this->redirect("5","/logout","<img src=\"/assets/img/loading.gif\">your session has ended..");
			exit;
		}
		
		//check apakah session_nya aktif?
		$time= explode( " ", microtime());
		$timeNow= (double)$time[1];

		$result_session = $this->get_last_record("select kode_pengguna, akses_terakhir, status from sesi_pengguna where sesi_id='".$_SESSION["login_session"]."'");
		$last_access = $result_session["akses_terakhir"];
		$session_username = $result_session["kode_pengguna"];
		$session_status	= $result_session["status"];

		if($session_username!=$_SESSION['login_nip']){
			
			$sql="delete from sesi_pengguna where sesi_id='".$_SESSION["login_session"]."'";
			$result=$db->Execute($sql);
			if(!$result) print $db->ErrorMsg();
			$this->redirect("5","/logout","<img src=\"/assets/img/loading.gif\">Invalid Information for your session..");
			die();
		}
		if($session_status=='0'){ // session di disabled karena ada user dengan login yang sama, login kembali!
			$sql="select akses_ip,ctime from sesi_pengguna where status='1' and kode_pengguna='".$_SESSION["login_nip"]."'";
			$row = $this->get_last_record($sql);
			$ip = $row["ip"];
			$ctime = $row["ctime"];
			
			$sql="delete from tbl_session where username='".$_SESSION['login_nip']."' and status='0'";
			$result=$db->Execute($sql);
			if(!$result) print $db->ErrorMsg();
			$this->redirect("5","/logout","<img src=\"/assets/img/loading.gif\">There is another people access your login..");
			exit;

		}

		if(empty($session_username)){
			$this->redirect("5","/logout","<img src=\"/assets/img/loading.gif\">Session expired..");
			exit;

		}
		
		//check apakah user idle selama lebih dari 1 jam?
		$diff   = $timeNow - $last_access;
		$limit  = 60*30;

		if($diff>$limit){
			$this->redirect("5","/logout","<img src=\"/assets/img/loading.gif\">Session expired..");
			exit;
		}

		$sql="update sesi_pengguna set akses_terakhir='".$timeNow."' where sesi_id='".$_SESSION["login_session"]."'";
		$result=$db->Execute($sql);
		if(!$result) print $db->ErrorMsg();
	
		
	}
	
	
	function detail_pengguna($user_id){
		$arr = array();
		if($user_id!=""){
			$arr = $this->get_last_record("SELECT a.*,b.nama_entitas,b.singkatan_entitas,c.nama_unit_kerja,c.singkatan_unit_kerja,d.nama_jabatan FROM pengguna AS a LEFT JOIN entitas AS b on a.kode_entitas_pengguna=b.kode_entitas left join unit_kerja AS c on a.kode_unit_kerja_pengguna=c.kode_unit_kerja left join jabatan as d on a.kode_jabatan_pengguna=d.kode_jabatan where a.kode_pengguna='".$user_id."' ");
		}
		return $arr;
	}

	function get_daftar_menu($hak_akses){
		global $db;
		
		$hak_akses = empty($hak_akses) ? "default" : $hak_akses;
		$rs_menu = array();
		$query_all_hak = "SELECT kode_menu FROM hak_akses_menu WHERE kode_hak='".$hak_akses."' ";
		$rs_anggota = $db->Execute($query_all_hak);
		$rs_menu = array();	
		while($row = $rs_anggota->fetchRow()){
			$rs_menu[] = $row['kode_menu'];
		}
		return $rs_menu;
	}
	function paging($count_page,$page,$start,$link,$extend_link=""){
		# --------------------------------------------- #
		## --- rule of link to next or prev page --- ##
		if(preg_match("/[=]/i",$link))
			$link .= "&";
		
		$count_page =($count_page=="" ? 1 : $count_page);
		$page = ($page=="" ? 1 : $page);
		if($page>=$count_page){
			$next = "<a href=\"".$link."/".$page.$extend_link."\" aria-label=\"Previous\"><i class=\"glyphicon glyphicon-triangle-right\"></i></a>";
		}
		else{
			$next = "<a href=\"".$link."/".($page+1).$extend_link."\" aria-label=\"Previous\"><i class=\"glyphicon glyphicon-triangle-right\"></i></a>";
		}

		if($page <=1) {
			$prev = "<a href=\"".$link."/".($page).$extend_link."\" aria-label=\"Previous\"><i class=\"glyphicon glyphicon-triangle-left\"></i></a>";
		}
		else{
			$prev = "<a href=\"".$link."/".($page-1).$extend_link."\" aria-label=\"Previous\"><i class=\"glyphicon glyphicon-triangle-left\"></i></a>";
		}	

		return "
				<nav aria-label=\"Page navigation\" class=\"pull-right\">
					<ul class=\"pagination\">
						<li>
							<a href=\"".$link."/1".$extend_link."\"  aria-label=\"Next\"><i class=\"glyphicon glyphicon-backward\" aria-hidden=\"true\"></i></a>
						</li>
						<li>					
							".$prev."
						</li>
						<li><a href=\"\">".$page." Of ".$count_page."</a></li>
						<li>					
							".$next."
						</li>
						<li>
							<a href=\"".$link."/".($count_page).$extend_link."\" aria-label=\"Previous\"><i class=\"glyphicon glyphicon-forward\"></i></a>
						</li>
					</ul>
				</nav>		
			   ";
		## ---------------------------------------- ##
		return $return;
	}
	
	function insert_log($activity,$detail){
		global $_SESSION,$db;
		
		$detail = base64_encode($detail);
		$query = "INSERT INTO log (kode_pengguna,aktifitas,detail,waktu) VALUES ('".$_SESSION['login_nip']."','".$activity."','".$detail."','".date("Y-m-d H:i:s")."')";
		$result = $db->Execute($query);	
		
	}
	
}


?>