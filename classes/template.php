<?php

class app_global extends _global{
	
	var $return_value;
	var $parameter;
	
	
	function __construct(){
		echo "
<!DOCTYPE html>
<html lang=\"en\">
<head>

    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">

    <title>Scorecard System</title>

    <!-- Bootstrap Core CSS -->
    <link href=\"/assets/css/bootstrap.min.css\" rel=\"stylesheet\">

    <!-- Custom CSS -->
    <link href=\"/assets/css/simple-sidebar.css\" rel=\"stylesheet\">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
        <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src=\"/assets/js/jquery.js\"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src=\"/assets/js/bootstrap.min.js\"></script>
	
    <!-- Validation JavaScript -->
    <script src=\"/assets/js/jquery_validation.js\"></script>
	
</head>

<body>
			 ";
	}
	
	function get_view($view_file=false){
		global $hak_akses_menu;
		
		$default_path = "/view/";
		
		if(!preg_match("/^php|html|htm|asp/i",$view_file))
			$view_file .= ".php";
	
		$file = $default_path.$view_file;
		
		if(file_exists(DOCUMENT_ROOT.$file)){
			include_once(DOCUMENT_ROOT.$file);
		}
		else{
			include_once(DOCUMENT_ROOT."/view/errors/404.php");			
		}
		
	}
	
		
	function footer(){
		global $navv;
		
		echo "

    <!-- Menu Toggle Script -->
    <script>
    $(\"#menu-toggle\").click(function(e) {
        e.preventDefault();
        $(\"#wrapper\").toggleClass(\"toggled\");
    });
	
	function check_menu(param){
		if($(\"#\"+param ).parent().parent().hasClass( \"collapse\" )){
			$(\"#\"+param ).parent().parent().removeClass( \"collapse\" );
			$(\"#\"+param ).parent().parent().addClass( \"collapse in\" );
		}
		$(\"#\"+param ).addClass( \"active\" );
			
	}
	check_menu(\"".$navv."\")
    </script>
</body>

</html>
			 ";
	}

	function simple_footer(){
		global $navv;
		
		echo "

    <!-- jQuery -->
    <script src=\"/assets/js/jquery.js\"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src=\"/assets/js/bootstrap.min.js\"></script>
	
    <!-- Validation JavaScript -->
    <script src=\"/assets/js/jquery_validation.js\"></script>
	
    </script>
</body>

</html>
			 ";
	}
	
	function test(){
		echo "<h2>test</h2>";
	}
	
}
?>