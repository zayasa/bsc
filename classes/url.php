<?php

class url{
	
	private $url;
	private $n_url;
	
	function __construct(){
		
		global $_GET;
		$array = array();
		foreach($_GET AS $key => $val){
			if(preg_match("/[\/\.]/i",$val)){
				$val = explode("/",$val);
			}
			
			if(is_array($val)){
				foreach($val AS $ky=>$vl){
					$array[$ky] = $vl;						
				}
			}
			else{
				$array[$key] = $val;					
			}
		}
		$this->url = $array;
		$this->url = $this->_reindex($this->url);
		
	}
	
	function get_url(Array $array = array()){
		$this->n_url = array();
		foreach($array AS $ky=>$vl){
			$this->n_url[$vl] = (isset($this->url[$ky]) ? $this->url[$ky] : "");
			unset($this->url[$ky]);
		}
		$this->url = $this->_reindex($this->url);
		if(count($this->n_url==1))
			return implode("",$this->n_url);
		else
			return $this->n_url;
	}
	
	function _reindex(Array $ar = array()){
		$newAr = array(); // your array
		foreach($ar as $one){
			$newAr[] = $one;
		}
		return $newAr;
	}
	
}
?>