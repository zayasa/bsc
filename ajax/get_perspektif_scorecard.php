<?php
require_once($_SERVER['DOCUMENT_ROOT']."/inc/app_global.php");

$contents = ob_get_contents();
ob_end_clean();
foreach($_GET AS $key=>$val){
	$key = strtolower($key);
	$$key = trim($val);
}

	$array_perspektif = array();

	$get_detail = $tpl->get_last_record("SELECT tanggal_mulai_periode, tanggal_selesai_periode FROM periode WHERE kode_periode='".$kode_periode."' ");
	$tgl_mulai = date("Y-m-d",strtotime($get_detail['tanggal_mulai_periode']));
	$tgl_selesai = date("Y-m-d",strtotime($get_detail['tanggal_selesai_periode']));
		
	
	$q_scorecard = "SELECT a.*,c.image_scorecard FROM scorecard	AS a LEFT JOIN tipe_scorecard AS c ON a.tipe_scorecard=c.kode_tipe_scorecard WHERE kode_periode_scorecard='".$kode_periode."' AND kode_pemilik_scorecard='".$kode_unit_kerja."' AND a.kode_induk_scorecard in (SELECT kode_scorecard FROM scorecard WHERE kode_periode_scorecard='".$kode_periode."' AND kode_pemilik_scorecard='".$kode_unit_kerja."' AND tipe_scorecard='SK') ORDER BY GetAncestry(kode_scorecard) ASC";
	$ls_scorecard = $db->Execute($q_scorecard);
	while($sc = $ls_scorecard->fetchRow()){
		foreach($sc AS $key=>$val){
			$key  = strtolower($key);
			$$key = trim($val);
		}
		
		if($tipe_polarisasi_scorecard=="min" || $tipe_polarisasi_scorecard=="max" || $tipe_polarisasi_scorecard=="avg" || $tipe_polarisasi_scorecard=="sum"){
			$aggr = strtoupper($tipe_polarisasi_scorecard);
			$tablee = "scorecard_nilai a";
		}
		else if($tipe_polarisasi_scorecard=="1st" || $tipe_polarisasi_scorecard=="lst"){
			$aggr = "MAX";
			$tablee = "
						scorecard_nilai a
						inner join (
							select a.kode_scorecard,".($tipe_polarisasi_scorecard=="1st" ? "MIN" : "MAX")."(a.id_periode_waktu) as MaxDate
							from scorecard_nilai AS a
							left join periode_waktu AS b 
								on a.id_periode_waktu=b.id
								where a.kode_scorecard='".$kode_scorecard."'
								AND b.tanggal_mulai>='".$tgl_mulai."' AND b.tanggal_selesai<='".$tgl_selesai."'
							group by kode_scorecard
						) tm on a.kode_scorecard = tm.kode_scorecard and a.id_periode_waktu= tm.MaxDate			
					  ";
			
			
			
		}
	
			$query = "
					SELECT 
						".$aggr."(a._TAR) AS _TAR,
						".$aggr."(a._RAL) AS _RAL,
						".$aggr."(a._MIN) AS _MIN,
						".$aggr."(a._MAX) AS _MAX,
						".$aggr."(a._BSE) AS _BSE,
						".$aggr."(a._SCO) AS _SCO
					FROM
						(
						SELECT
							a.id_periode_waktu,
							a.kode_scorecard,
							a.tipe_nilai_scorecard,
							IF (
								a.tipe_nilai_scorecard = 'TAR',
								a.nilai_scorecard,
								NULL
							) AS _TAR,

							IF (
								a.tipe_nilai_scorecard = 'RAL',
								a.nilai_scorecard,
								NULL
							) AS _RAL,

							IF (a.tipe_nilai_scorecard = 'SCO',
							 a.nilai_scorecard,
							 NULL
							) AS _SCO,

							IF (
								a.tipe_nilai_scorecard = 'MIN',
								a.nilai_scorecard,
								NULL
							) AS _MIN,

							IF (
								a.tipe_nilai_scorecard = 'MAX',
								a.nilai_scorecard,
								NULL
							) AS _MAX,

							IF (
								a.tipe_nilai_scorecard = 'BSE',
								a.nilai_scorecard,
								NULL
							) AS _BSE
						FROM
							".$tablee."
						LEFT JOIN 
							periode_waktu b
						ON 
							a.id_periode_waktu=b.id
						WHERE 
							IFNULL(a.data_dasar,'')<>'1'
						AND 
							a.kode_scorecard='".$kode_scorecard."'
						AND b.tanggal_selesai<='".$tgl_selesai."' AND b.tanggal_mulai>='".$tgl_mulai."'
					) AS a
					GROUP BY a.kode_scorecard			
					 ";
		//echo $query."<hr>";
		$list_nilai = $tpl->get_last_record($query);
		
		/* get label nilai */
		$q_label_nilai = "SELECT * FROM label_nilai WHERE kode_label_nilai='".$label_nilai_scorecard."' ";
		$arr_label_nilai = $tpl->get_last_record($q_label_nilai);
		
		foreach($list_nilai AS $ky =>$vl){
			$ky  = strtolower($ky);
			
				$prefix = "";
				$sufix = "";
				/**/
				if($satuan_nilai_scorecard=="num" && $vl!=""){
					if(is_decimal($vl)===true)
						$vl = number_format($vl, 2, ',', '.');
					else 
						$vl = number_format($vl, 0, ',', '.');
				}
				else if($satuan_nilai_scorecard=="tgll"  && $vl!=""){
					$vl = date("d/m/Y",strtotime($vl));
				}
				else if($vl=="" && ($satuan_nilai_scorecard=="stri" || $satuan_nilai_scorecard=="tgll")){
					$vl = "NULL";
				}
				else if($vl=="" && $satuan_nilai_scorecard=="num")
					$vl = "0";
				
				if($arr_label_nilai['penempatan_label']=="1")
					$prefix = $arr_label_nilai['lambang_label_nilai']." ";
				
				if($arr_label_nilai['penempatan_label']=="2")
					$sufix = " ".$arr_label_nilai['lambang_label_nilai'];
			
			if(!preg_match("/sco/i",$ky))
				$$ky = $prefix.trim($vl).$sufix;
			else 
				$$ky = trim($vl);
		}
		
		
		$array_perspektif[] = array("y"=>$nama_scorecard,"a"=>$_tar,"b"=>$_sco);
		/*
		echo "<tr class=\"treegrid-".$kode_scorecard." ".($kode_induk_scorecard=="" ? "" : "treegrid-parent-".$kode_induk_scorecard )."\">";
		echo "<td><img src='".$image_scorecard."' width=\"13\"> <span class=\"nama_scorecard\" >".$nama_scorecard."</span></td><td>".$_tar."</td><td>".$_ral."</td><td>".$_sco."</td><td class=\"text-right\"><a href=\"/".$controller."/".$kode_periode."/".$kode_unit_kerja."/daftar-nilai/".$kode_scorecard."\" title=\"ubah nilai\" type=\"button\" class=\"btn btn-default btn-xs\"><span class=\"glyphicon glyphicon-list-alt\" aria-hidden=\"true\"></span></a>&nbsp;<a href=\"/".$controller."/".$kode_periode."/".$kode_unit_kerja."/ubah/".$kode_scorecard."\" type=\"button\" class=\"btn btn-default btn-xs\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span></a>&nbsp;<a href=\"/".$controller."/".$kode_periode."/".$kode_unit_kerja."/tambah/".$kode_scorecard."\" type=\"button\" class=\"btn btn-default btn-xs\"><span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span></a></td>";
		echo "</tr>";
		*/
	}
	
function encodeURIComponent($str){
	$revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
	return strtr(rawurlencode($str), $revert);
}

echo encodeURIComponent(json_encode($array_perspektif));
//$tpl->pre($array_perspektif);
?>