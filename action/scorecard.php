<?php

$error = "";
$button = array("kembali","/".$controller."/".$kode_periode."/".$kode_unit_kerja);
$array_post = array();
$message = "";
$columns = "";
$values = "";
$list = "";
$_kode_scorecard = $url->get_url(array('kode_scorecard'));


/* convert post value */
foreach($_POST AS $key=>$val){
	if(!is_array($val)){
		
		$$key = $val;
		
		if($action=="update"){
			if(preg_match("/tanggal|tgl/i",$key) && $val=="")
				$list .= $key."=NULL,";
			else if($key!="kode_scorecard")
				$list .= $key."='".$val."',";
		}
		else{
			$columns .= $key.",";
			if(preg_match("/tanggal|tgl/i",$key) && $val=="")
				$values .= "NULL,";
			else
				$values .= "'".$val."',";
		}
		
	}
	else{
		$array_post[$key] = $val;
	}
}
//die($tpl->pre($array_post));
	
if($action=="update"){
	
	if($tpl->check_exist_value("SELECT kode_scorecard FROM scorecard WHERE kode_scorecard='".$kode_scorecard."' ")===false){
		$error =  "Scorecard tidak ditemukan!";
	}
	else{
		$query_operation = "UPDATE scorecard SET ".$list."nama_ubah='".$detail_login['kode_pengguna']."', tanggal_ubah='".date("Y-m-d H:i:s")."' , kode_urutan_scorecard='".($kode_induk_scorecard=="" ?  $kode_scorecard : $kode_induk_scorecard)."' WHERE kode_scorecard='".$kode_scorecard."' ";
		$message = "Scorecard berhasil dibarui!";
		$tpl->insert_log("ubah scorecard ".$kode_scorecard,$query_operation);
	}
	
	
}
else if($action=="simpan"){
		
	/* check validasi */
	if($tpl->check_exist_value("SELECT kode_scorecard FROM scorecard WHERE kode_scorecard='".$kode_scorecard."' ")===true){
		$error =  "Kode Scorecard sudah terpakai!";
	}
	else{
		$query_operation = "INSERT INTO scorecard (".$columns."nama_buat,tanggal_buat,kode_urutan_scorecard) VALUES (".$values."'".$detail_login['kode_scorecard']."','".date("Y-m-d H:i:s")."','".($kode_induk_scorecard=="" ? $kode_scorecard : $kode_induk_scorecard)."')";
		$message = "Scorecard berhasil ditambah!";
		
		$tpl->insert_log("tambah scorecard ".$kode_scorecard,$query_operation);
	}
	
}
else{
	
	if($tpl->check_exist_value("SELECT kode_scorecard FROM scorecard WHERE kode_scorecard='".$_kode_scorecard."' ")===false){
		$error =  "Scorecard tidak ditemukan!";
	}
	else{
		$query_operation = "DELETE FROM scorecard WHERE kode_scorecard='".$_kode_scorecard."' ";
		$message = "Scorecard berhasil dihapus!";
		$tpl->insert_log("hapus scorecard ".$_kode_scorecard,$query_operation);
	}
	
}

if($error!=""){
	$tpl->alert_panel("Validasi Form!",$error,"");
}
else{
	$exec = $db->Execute($query_operation);
	if(!$exec){
		$error = "Periksa kembali inputan anda!";
	}
	else{
		
		if($action=="simpan" || $action=="update"){
			$urutan = $db->getOne("SELECT GetAncestry('".$kode_induk_scorecard."')");
			$n_urutan = (empty($urutan) ? $kode_scorecard : ",".$kode_scorecard);
			$urutan .= $n_urutan;
			$exec = $db->Execute("UPDATE scorecard SET kode_urutan_scorecard='".$urutan."' WHERE kode_scorecard='".$kode_scorecard."' ");
			
		}
		
		$columnss = "";
		$valuess = array();
		foreach($array_post AS $key=>$val){
			$no = 1;
			$columnss .= $key.",";
			
			foreach($val AS $k=>$v){
				$valuess[$k] .= "'".$v."',";
				$no++;
			}
		}
		
		foreach($valuess AS $kk=>$vv){
			// check data 
			$q_delete = "DELETE FROM scorecard_nilai WHERE kode_scorecard='".$kode_scorecard."' AND data_dasar='1' AND tipe_nilai_scorecard='".$kk."' ";
			$delete = $db->Execute($q_delete);
			
			$q_insert = "INSERT INTO scorecard_nilai (".$columnss."kode_scorecard,nama_buat,tanggal_buat,data_dasar) VALUES (".$vv."'".$kode_scorecard."','".$detail_login['kode_pengguna']."',SYSDATE(),'1')";
			$insert = $db->Execute($q_insert);
			$tpl->insert_log("insert nilai ".$kk." scorecard  ".$kode_scorecard,$q_insert);
		}
		$cond = " AND a.DATA_DASAR='1' ";
		include("/action/kalkulasi_nilai_scorecard.php");
		
	}
	
	
	if($error!=""){
		$tpl->alert_panel("Validasi Form!",$error,"");
	}
	else{
		$tpl->alert_panel("Informasi",$message,$button,"info");	
	}
}

?>