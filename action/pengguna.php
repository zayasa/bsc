<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$error = "";
$button = array("kembali","/pengguna");
$message = "";
$columns = "";
$values = "";
$list = "";
$_kode_pengguna = $url->get_url(array('kode_pengguna'));


/* convert post value */
foreach($_POST AS $key=>$val){
	$$key = $val;
	
	if($action=="update"){
		if(preg_match("/tanggal|tgl/i",$key) && $val=="")
			$list .= $key."=NULL,";
		else if($key!="kode_pengguna")
			$list .= $key."='".$val."',";
	}
	else{
		$columns .= $key.",";
		if(preg_match("/tanggal|tgl/i",$key) && $val=="")
			$values .= "NULL,";
		else
			$values .= "'".$val."',";
	}
	
}

	
if($action=="update"){
	
	if($tpl->check_exist_value("SELECT kode_pengguna FROM pengguna WHERE kode_pengguna='".$kode_pengguna."' ")===false){
		$error =  "Pengguna tidak ditemukan!";
	}
	else{
		$query_operation = "UPDATE pengguna SET ".$list."nama_ubah='".$detail_login['kode_pengguna']."', tanggal_ubah='".date("Y-m-d H:i:s")."' WHERE kode_pengguna='".$kode_pengguna."' ";
		$message = "Pengguna berhasil dibarui!";
		$tpl->insert_log("ubah pengguna ".$kode_pengguna,$query_operation);
	}
	
	
}
else if($action=="reset"){
		
	/* check validasi */	
	if($tpl->check_exist_value("SELECT kode_pengguna FROM pengguna WHERE kode_pengguna='".$_kode_pengguna."' ")===false){
		$error =  "Pengguna tidak ditemukan!";
	}
	else{
		$tanggal_lahir_pengguna = $db->getOne("SELECT tanggal_lahir_pengguna FROM pengguna WHERE kode_pengguna='".$_kode_pengguna."' ");
		$query_operation = "UPDATE pengguna SET sandi_pengguna='".md5(str_replace("-","",$tanggal_lahir_pengguna))."'  WHERE kode_pengguna='".$_kode_pengguna."' ";
		$message = "Sandi pengguna berhasil diatur ulang!<br>Gunakan sandi default tanggal lahir anda yyyymmdd";
		$tpl->insert_log("reset sandi pengguna ".$_kode_pengguna,$query_operation);
	}
	
}
else if($action=="simpan"){
		
	/* check validasi */
	if($tpl->check_exist_value("SELECT kode_pengguna FROM pengguna WHERE kode_pengguna='".$kode_pengguna."' ")===true){
		$error =  "Kode Pengguna sudah terpakai!";
	}
	else{
		$sandi_pengguna = md5(str_replace("-","",$tanggal_lahir_pengguna));
		$query_operation = "INSERT INTO pengguna (".$columns."sandi_pengguna,nama_buat,tanggal_buat) VALUES (".$values."'".$sandi_pengguna."','".$detail_login['kode_pengguna']."','".date("Y-m-d H:i:s")."')";
		$message = "Pengguna berhasil ditambah!<br>Gunakan sandi default tanggal lahir anda yyyymmdd";
		
		$tpl->insert_log("tambah pengguna ".$kode_pengguna,$query_operation);
	}
	
}
else{
	
	if($tpl->check_exist_value("SELECT kode_pengguna FROM pengguna WHERE kode_pengguna='".$_kode_pengguna."' ")===false){
		$error =  "Pengguna tidak ditemukan!";
	}
	else{
		$query_operation = "DELETE FROM pengguna WHERE kode_pengguna='".$_kode_pengguna."' ";
		$message = "Pengguna berhasil dihapus!";
		$tpl->insert_log("hapus pengguna ".$_kode_pengguna,$query_operation);
	}
	
}

if($error!=""){
	$tpl->alert_panel("Validasi Form!",$error,"");
}
else{

	$exec = $db->Execute($query_operation);
	if(!$exec){
		$error = "Periksa kembali inputan anda!";
	}
	
	
	if($error!=""){
		$tpl->alert_panel("Validasi Form!",$error,"");
	}
	else{
		$tpl->alert_panel("Informasi",$message,$button,"info");	
	}
}

?>