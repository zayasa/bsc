<?php

$query_scorecard_nilai = "
						SELECT
							a.*
						FROM
							scorecard_nilai AS a
						LEFT JOIN 
							tipe_nilai AS b 
						ON 
							a.tipe_nilai_scorecard=b.tipe_nilai
						LEFT JOIN 
							scorecard AS d 
						ON 
							a.kode_scorecard = d.kode_scorecard
						LEFT JOIN 
							tipe_scorecard AS c 
						ON 
							c.kode_tipe_scorecard = d.tipe_scorecard
						WHERE 
							(
								a.formula_nilai_scorecard LIKE '%".$kode_scorecard."%'
							OR 
								a.kode_scorecard='".$kode_scorecard."'
							OR 
								(SELECT kode_urutan_scorecard from scorecard where kode_scorecard='".$kode_scorecard."') LIKE CONCAT('%',a.kode_scorecard,'%')
							)
						".$cond."
						ORDER BY 
							b.kelompok_nilai ASC,
							c.no_urut_tipe_scorecard DESC							
							";
//echo $kode_scorecard."<br>".$query_scorecard_nilai."<hr>";
$scorecard_nilai = $db->Execute($query_scorecard_nilai);
while($snn = $scorecard_nilai->fetchRow()){

	if($snn['formula_nilai_scorecard']!=""){
		/*
		$from_scorecard_nilai = "";
		$query_daftar_scorecard_nilai = "
								SELECT
									a.*,
									b.satuan_nilai_scorecard
								FROM
									scorecard_nilai AS a
								LEFT JOIN
									scorecard AS b 
								ON 
									a.kode_scorecard=b.kode_scorecard
								".$cond1."
								 ";
		echo $query_daftar_scorecard_nilai."]]<hr>";
		$daftar_scorecard_nilai = $db->Execute($query_daftar_scorecard_nilai);
		while($sn = $daftar_scorecard_nilai->fetchRow()){
			foreach($sn AS $key=>$val){
				$key  = "pr_".strtolower($key);
				$$key = trim($val);
			}
			if($pr_satuan_nilai_scorecard=="num"){
				$as_nilai = (empty($pr_nilai_scorecard) ? "NULL" : $pr_nilai_scorecard);
			}
			else{
				$as_nilai = "'".$pr_nilai_scorecard."'";		
			}
			$from_scorecard_nilai .= $as_nilai." AS ".$pr_kode_scorecard."_".$pr_tipe_nilai_scorecard." ,";
		}
		$from_scorecard_nilai = substr($from_scorecard_nilai,0,-1);
	
		unset($value);
		$_query = "
					SELECT (".$snn['formula_nilai_scorecard'].")
					FROM ( SELECT ".$from_scorecard_nilai.") AS a
				  ";
		echo $snn['kode_scorecard']." => ".$_query."<hr>";
		$value = $db->getOne($_query);
		if($value){
			$q_update = "UPDATE scorecard_nilai SET nilai_scorecard='".$value."' WHERE kode_nilai_scorecard='".$snn['kode_nilai_scorecard']."' ";
			$update = $db->Execute($q_update);			
			
		}
		*/
		$from_scorecard_nilai = "";
		$q_scorecard = "SELECT a.*,c.image_scorecard FROM scorecard	AS a LEFT JOIN tipe_scorecard AS c ON a.tipe_scorecard=c.kode_tipe_scorecard WHERE kode_periode_scorecard='".$kode_periode."' ORDER BY GetAncestry(kode_scorecard) ASC";
		//echo $q_scorecard;
		$ls_scorecard = $db->Execute($q_scorecard);
		while($sc = $ls_scorecard->fetchRow()){
			foreach($sc AS $key=>$val){
				$key  = strtolower($key);
				$$key = trim($val);
			}
			$aggr = "";
			$aggr1 = "";
			if($tipe_polarisasi_scorecard=="min" || $tipe_polarisasi_scorecard=="max" || $tipe_polarisasi_scorecard=="avg" || $tipe_polarisasi_scorecard=="sum"){
				$aggr = strtoupper($tipe_polarisasi_scorecard);
				$tablee = "scorecard_nilai a";
				$aggr1 = strtoupper($tipe_polarisasi_scorecard);
			}
			else if($tipe_polarisasi_scorecard=="1st" || $tipe_polarisasi_scorecard=="lst"){
				$aggr = "MAX";
				$aggr1 = "";
				$tablee = "
							scorecard_nilai a
							inner join (
								select a.kode_scorecard,".($tipe_polarisasi_scorecard=="1st" ? "MIN" : "MAX")."(a.id_periode_waktu) as MaxDate
								from scorecard_nilai AS a
								left join periode_waktu AS b 
									on a.id_periode_waktu=b.id
									where a.kode_scorecard='".$kode_scorecard."'
									AND b.tanggal_mulai>='".$get_detail['tanggal_mulai']."' AND b.tanggal_selesai<='".$get_detail['tanggal_selesai']."'
								group by kode_scorecard
							) tm on a.kode_scorecard = tm.kode_scorecard and a.id_periode_waktu= tm.MaxDate			
						  ";
				
				
				
			}
		
				$query = "
						SELECT 
							a.kode_scorecard,
							".$aggr."(a._TAR) AS _TAR,
							".$aggr."(a._RAL) AS _RAL,
							".$aggr."(a._MIN) AS _MIN,
							".$aggr."(a._MAX) AS _MAX,
							".$aggr."(a._BSE) AS _BSE,
							".$aggr."(a._SCO) AS _SCO
						FROM
							(
							SELECT
								a.id_periode_waktu,
								a.kode_scorecard,
								a.tipe_nilai_scorecard,
								IF (
									a.tipe_nilai_scorecard = 'TAR',
									(a.nilai_scorecard),
									NULL
								) AS _TAR,

								IF (
									a.tipe_nilai_scorecard = 'RAL',
									(a.nilai_scorecard),
									NULL
								) AS _RAL,

								IF (a.tipe_nilai_scorecard = 'SCO',
								 (a.nilai_scorecard),
								 NULL
								) AS _SCO,

								IF (
									a.tipe_nilai_scorecard = 'MIN',
									(a.nilai_scorecard),
									NULL
								) AS _MIN,

								IF (
									a.tipe_nilai_scorecard = 'MAX',
									(a.nilai_scorecard),
									NULL
								) AS _MAX,

								IF (
									a.tipe_nilai_scorecard = 'BSE',
									(a.nilai_scorecard),
									NULL
								) AS _BSE
							FROM
								".$tablee."
							LEFT JOIN 
								periode_waktu b
							ON 
								a.id_periode_waktu=b.id
							WHERE 
								IFNULL(a.data_dasar,'')<>'1'
							AND 
								a.kode_scorecard='".$kode_scorecard."'
							AND b.tanggal_selesai<='".$get_detail['tanggal_selesai']."' AND b.tanggal_mulai>='".$get_detail['tanggal_mulai']."'
							AND a.status_nilai_scorecard='2'
						) AS a
						GROUP BY a.kode_scorecard			
						 ";
			//if($kode_scorecard=="IK00000069" || $kode_scorecard=="IK00000052")
				//echo $query."<hr>";
			$list_nilai = $tpl->get_last_record($query);
			
			//$tpl->pre($list_nilai);
			foreach($list_nilai AS $key=>$val){
				$key  = strtolower($key);
				
				if(!preg_match("/kode_scorecard/i",$key)){
					
					if($val=="")
						if(!preg_match("/kode_scorecard/i",$key) && ($satuan_nilai_scorecard=="stri" || $satuan_nilai_scorecard=="tgll" ))
							$val = "NULL";
						else 
							$val = "0";
						
					else{
						if(!preg_match("/kode_scorecard/i",$key) && $satuan_nilai_scorecard!="num" && $satuan_nilai_scorecard!="tgll" )
							$val = "'".$val."'";
						else 
							$val = $val;
					}
					
				}
				$list_nilai[$key] = $val;
				
			}
			//$tpl->pre($list_nilai);
			if($list_nilai['kode_scorecard']!="") 
				$from_scorecard_nilai .= $list_nilai['_tar']." AS ".$list_nilai['kode_scorecard']."_TAR , ".$list_nilai['_ral']." AS ".$list_nilai['kode_scorecard']."_RAL , ".$list_nilai['_sco']." AS ".$list_nilai['kode_scorecard']."_SCO ,";
			
		}

		$from_scorecard_nilai = substr($from_scorecard_nilai,0,-1);
		unset($value);
		$_query = "
					SELECT (".$snn['formula_nilai_scorecard'].")
					FROM ( SELECT ".$from_scorecard_nilai.") AS a
				  ";
		//echo $snn['kode_scorecard']." => ".$_query."<hr>";
		$value = $db->getOne($_query);
		if($value){
			$q_update = "UPDATE scorecard_nilai SET nilai_scorecard='".$value."' WHERE kode_nilai_scorecard='".$snn['kode_nilai_scorecard']."' ";
			$update = $db->Execute($q_update);			
			
		}
		
	}
}

						 
?>