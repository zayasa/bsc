<?php

$error = "";
$button = array("kembali","/hak-akses");
$message = "";
$columns = "";
$values = "";
$list = "";
$_kode_hak = $url->get_url(array('kode_hak'));


/* convert post value */
foreach($_POST AS $key=>$val){
	$$key = $val;
	if($key!="menu"){
		if($action=="update"){
			if($key!="kode_hak")
				$list .= $key."='".$val."',";
		}
		else{
			$columns .= $key.",";
			$values .= "'".$val."',";
		}		
	}
}

$columns = substr($columns,0,-1);	
$values = substr($values,0,-1);	
$list = substr($list,0,-1);	
if($action=="update"){
	
	if($tpl->check_exist_value("SELECT kode_hak FROM hak_akses WHERE kode_hak='".$kode_hak."' ")===false){
		$error =  "Hak akses tidak ditemukan!";
	}
	else{
		$query_operation = "UPDATE hak_akses SET ".$list." WHERE kode_hak='".$kode_hak."' ";
		$message = "Hak akses berhasil dibarui!";
		$tpl->insert_log("ubah hak_akses ".$kode_hak,$query_operation);
	}
	
	
}
else if($action=="simpan"){
		
	/* check validasi */
	if($tpl->check_exist_value("SELECT kode_hak FROM hak_akses WHERE kode_hak='".$kode_hak."' ")===true){
		$error =  "Kode hak_akses sudah terpakai!";
	}
	else{
		$query_operation = "INSERT INTO hak_akses (".$columns.") VALUES (".$values.")";
		$message = "Hak akses berhasil ditambah!";
		
		$tpl->insert_log("tambah hak_akses ".$kode_hak,$query_operation);
	}
	
}
else{
	
	if($tpl->check_exist_value("SELECT kode_hak FROM hak_akses WHERE kode_hak='".$_kode_hak."' ")===false){
		$error =  "Hak akses tidak ditemukan!";
	}
	else{
		$query_operation = "DELETE FROM hak_akses WHERE kode_hak='".$_kode_hak."' ";
		$message = "Hak akses berhasil dihapus!";
		$tpl->insert_log("hapus hak_akses ".$_kode_hak,$query_operation);
	}
	
}

if($error!=""){
	$tpl->alert_panel("Validasi Form!",$error,"");
}
else{
	
	$exec = $db->Execute($query_operation);
	if(!$exec){
		$error = "Periksa kembali inputan anda!";
	}
	else{
		/* kosongkan dulu */
		$kode_hak = ($_kode_hak!="" ? $_kode_hak : $kode_hak);
		$query_delete = "DELETE FROM hak_akses_menu where kode_hak='".$kode_hak."' ";
		$result = $db->Execute($query_delete);
		foreach($menu AS $val){
			$q_insert = "INSERT INTO hak_akses_menu VALUES ('".$kode_hak."','".$val."')";
			$insert = $db->Execute($q_insert);
		}
	}
	
	
	if($error!=""){
		$tpl->alert_panel("Validasi Form!",$error,"");
	}
	else{
		$tpl->alert_panel("Informasi",$message,$button,"info");	
	}
}

?>