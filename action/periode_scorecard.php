<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$error = "";
$button = array("kembali","/periode-scorecard");
$message = "";
$columns = "";
$values = "";
$list = "";
$_kode_periode = $url->get_url(array('kode_periode'));


/* convert post value */
foreach($_POST AS $key=>$val){
	$$key = $val;
	
	if($action=="update"){
		if(preg_match("/tanggal|tgl/i",$key) && $val=="")
			$list .= $key."=NULL,";
		else if($key!="kode_periode")
			$list .= $key."='".$val."',";
	}
	else{
		$columns .= $key.",";
		if(preg_match("/tanggal|tgl/i",$key) && $val=="")
			$values .= "NULL,";
		else
			$values .= "'".$val."',";
	}
	
}

	
if($action=="update"){
	
	if($tpl->check_exist_value("SELECT kode_periode FROM periode WHERE kode_periode='".$kode_periode."' ")===false){
		$error =  "periode tidak ditemukan!";
	}
	else{
		$query_operation = "UPDATE periode SET ".$list."nama_ubah='".$detail_login['kode_pengguna']."', tanggal_ubah='".date("Y-m-d H:i:s")."' WHERE kode_periode='".$kode_periode."' ";
		$message = "periode berhasil dibarui!";
		$tpl->insert_log("ubah periode ".$kode_periode,$query_operation);
	}
	
	
}
else if($action=="simpan"){
		
	/* check validasi */
	if($tpl->check_exist_value("SELECT kode_periode FROM periode WHERE kode_periode='".$kode_periode."' ")===true){
		$error =  "Kode periode sudah terpakai!";
	}
	else{
		$query_operation = "INSERT INTO periode (".$columns."nama_buat,tanggal_buat) VALUES (".$values."'".$detail_login['kode_pengguna']."','".date("Y-m-d H:i:s")."')";
		$message = "periode berhasil ditambah!";
		
		$tpl->insert_log("tambah periode ".$kode_periode,$query_operation);
	}
	
}
else{
	
	if($tpl->check_exist_value("SELECT kode_periode FROM periode WHERE kode_periode='".$_kode_periode."' ")===false){
		$error =  "periode tidak ditemukan!";
	}
	else{
		$query_operation = "DELETE FROM periode WHERE kode_periode='".$_kode_periode."' ";
		$message = "periode berhasil dihapus!";
		$tpl->insert_log("hapus periode ".$_kode_periode,$query_operation);
	}
	
}

if($error!=""){
	$tpl->alert_panel("Validasi Form!",$error,"");
}
else{

	$exec = $db->Execute($query_operation);
	if(!$exec){
		$error = "Periksa kembali inputan anda!";
	}
	
	
	if($error!=""){
		$tpl->alert_panel("Validasi Form!",$error,"");
	}
	else{
		$tpl->alert_panel("Informasi",$message,$button,"info");	
	}
}

?>