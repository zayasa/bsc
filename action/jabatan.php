<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$error = "";
$button = array("kembali","/jabatan");
$message = "";
$columns = "";
$values = "";
$list = "";
$_kode_jabatan = $url->get_url(array('kode_jabatan'));


/* convert post value */
foreach($_POST AS $key=>$val){
	$$key = $val;
	
	if($action=="update"){
		if(preg_match("/tanggal|tgl/i",$key) && $val=="")
			$list .= $key."=NULL,";
		else if($key!="kode_jabatan")
			$list .= $key."='".$val."',";
	}
	else{
		$columns .= $key.",";
		if(preg_match("/tanggal|tgl/i",$key) && $val=="")
			$values .= "NULL,";
		else
			$values .= "'".$val."',";
	}
	
}

	
if($action=="update"){
	
	if($tpl->check_exist_value("SELECT kode_jabatan FROM jabatan WHERE kode_jabatan='".$kode_jabatan."' ")===false){
		$error =  "jabatan tidak ditemukan!";
	}
	else{
		$query_operation = "UPDATE jabatan SET ".$list."nama_ubah='".$detail_login['kode_pengguna']."', tanggal_ubah='".date("Y-m-d H:i:s")."' WHERE kode_jabatan='".$kode_jabatan."' ";
		$message = "jabatan berhasil dibarui!";
		$tpl->insert_log("ubah jabatan ".$kode_jabatan,$query_operation);
	}
	
	
}
else if($action=="simpan"){
		
	/* check validasi */
	if($tpl->check_exist_value("SELECT kode_jabatan FROM jabatan WHERE kode_jabatan='".$kode_jabatan."' ")===true){
		$error =  "Kode jabatan sudah terpakai!";
	}
	else{
		$query_operation = "INSERT INTO jabatan (".$columns."nama_buat,tanggal_buat) VALUES (".$values."'".$detail_login['kode_pengguna']."','".date("Y-m-d H:i:s")."')";
		$message = "jabatan berhasil ditambah!";
		
		$tpl->insert_log("tambah jabatan ".$kode_jabatan,$query_operation);
	}
	
}
else{
	
	if($tpl->check_exist_value("SELECT kode_jabatan FROM jabatan WHERE kode_jabatan='".$_kode_jabatan."' ")===false){
		$error =  "jabatan tidak ditemukan!";
	}
	else{
		$query_operation = "DELETE FROM jabatan WHERE kode_jabatan='".$_kode_jabatan."' ";
		$message = "jabatan berhasil dihapus!";
		$tpl->insert_log("hapus jabatan ".$_kode_jabatan,$query_operation);
	}
	
}

if($error!=""){
	$tpl->alert_panel("Validasi Form!",$error,"");
}
else{

	$exec = $db->Execute($query_operation);
	if(!$exec){
		$error = "Periksa kembali inputan anda!";
	}
	
	
	if($error!=""){
		$tpl->alert_panel("Validasi Form!",$error,"");
	}
	else{
		$tpl->alert_panel("Informasi",$message,$button,"info");	
	}
}

?>