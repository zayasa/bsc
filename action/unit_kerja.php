<?php

$error = "";
$button = array("kembali","/unit-kerja");
$message = "";
$columns = "";
$values = "";
$list = "";
$_kode_unit_kerja = $url->get_url(array('kode_unit_kerja'));


/* convert post value */
foreach($_POST AS $key=>$val){
	$$key = $val;
	
	if($action=="update"){
		if(preg_match("/tanggal|tgl/i",$key) && $val=="")
			$list .= $key."=NULL,";
		else if($key!="kode_unit_kerja")
			$list .= $key."='".$val."',";
	}
	else{
		$columns .= $key.",";
		if(preg_match("/tanggal|tgl/i",$key) && $val=="")
			$values .= "NULL,";
		else
			$values .= "'".$val."',";
	}
	
}

	
if($action=="update"){
	
	if($tpl->check_exist_value("SELECT kode_unit_kerja FROM unit_kerja WHERE kode_unit_kerja='".$kode_unit_kerja."' ")===false){
		$error =  "unit kerja tidak ditemukan!";
	}
	else{
		$query_operation = "UPDATE unit_kerja SET ".$list."nama_ubah='".$detail_login['kode_pengguna']."', tanggal_ubah='".date("Y-m-d H:i:s")."' WHERE kode_unit_kerja='".$kode_unit_kerja."' ";
		$message = "unit kerja berhasil dibarui!";
		$tpl->insert_log("ubah unit_kerja ".$kode_unit_kerja,$query_operation);
	}
	
	
}
else if($action=="simpan"){
		
	/* check validasi */
	if($tpl->check_exist_value("SELECT kode_unit_kerja FROM unit_kerja WHERE kode_unit_kerja='".$kode_unit_kerja."' ")===true){
		$error =  "Kode unit kerja sudah terpakai!";
	}
	else if($tpl->check_exist_value("SELECT singkatan_unit_kerja FROM unit_kerja WHERE singkatan_unit_kerja='".$singkatan_unit_kerja."' ")===true){
		$error =  "Singkatan unit kerja sudah terdaftar!";	
	}
	else{
		$query_operation = "INSERT INTO unit_kerja (".$columns."nama_buat,tanggal_buat) VALUES (".$values."'".$detail_login['kode_pengguna']."','".date("Y-m-d H:i:s")."')";
		$message = "unit kerja berhasil ditambah!";
		
		$tpl->insert_log("tambah unit_kerja ".$kode_unit_kerja,$query_operation);
	}
	
}
else{
	
	if($tpl->check_exist_value("SELECT kode_unit_kerja FROM unit_kerja WHERE kode_unit_kerja='".$_kode_unit_kerja."' ")===false){
		$error =  "unit kerja tidak ditemukan!";
	}
	else{
		$query_operation = "DELETE FROM unit_kerja WHERE kode_unit_kerja='".$_kode_unit_kerja."' ";
		$message = "unit kerja berhasil dihapus!";
		$tpl->insert_log("hapus unit_kerja ".$_kode_unit_kerja,$query_operation);
	}
	
}

if($error!=""){
	$tpl->alert_panel("Validasi Form!",$error,"");
}
else{

	$exec = $db->Execute($query_operation);
	if(!$exec){
		$error = "Periksa kembali inputan anda!";
	}
	
	
	if($error!=""){
		$tpl->alert_panel("Validasi Form!",$error,"");
	}
	else{
		$tpl->alert_panel("Informasi",$message,$button,"info");	
	}
}

?>