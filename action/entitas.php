<?php

$error = "";
$button = array("kembali","/entitas");
$message = "";
$columns = "";
$values = "";
$list = "";
$_kode_entitas = $url->get_url(array('kode_entitas'));


/* convert post value */
foreach($_POST AS $key=>$val){
	$$key = $val;
	
	if($action=="update"){
		if($key!="kode_entitas")
			$list .= $key."='".$val."',";
	}
	else{
		$columns .= $key.",";
		$values .= "'".$val."',";
	}
	
}

	
if($action=="update"){
	
	if($tpl->check_exist_value("SELECT kode_entitas FROM entitas WHERE kode_entitas='".$kode_entitas."' ")===false){
		$error =  "Entitas tidak ditemukan!";
	}
	else{
		$query_operation = "UPDATE entitas SET ".$list."nama_ubah='".$detail_login['kode_pengguna']."', tanggal_ubah='".date("Y-m-d H:i:s")."' WHERE kode_entitas='".$kode_entitas."' ";
		$message = "Entitas berhasil dibarui!";
		$tpl->insert_log("ubah entitas ".$kode_entitas,$query_operation);
	}
	
	
}
else if($action=="simpan"){
		
	/* check validasi */
	if($tpl->check_exist_value("SELECT kode_entitas FROM entitas WHERE kode_entitas='".$kode_entitas."' ")===true){
		$error =  "Kode entitas sudah terpakai!";
	}
	else if($tpl->check_exist_value("SELECT singkatan_entitas FROM entitas WHERE singkatan_entitas='".$singkatan_entitas."' ")===true){
		$error =  "Singkatan entitas sudah terdaftar!";	
	}
	else{
		$query_operation = "INSERT INTO entitas (".$columns."nama_buat,tanggal_buat) VALUES (".$values."'".$detail_login['kode_pengguna']."','".date("Y-m-d H:i:s")."')";
		$message = "Entitas berhasil ditambah!";
		
		$tpl->insert_log("tambah entitas ".$kode_entitas,$query_operation);
	}
	
}
else{
	
	if($tpl->check_exist_value("SELECT kode_entitas FROM entitas WHERE kode_entitas='".$_kode_entitas."' ")===false){
		$error =  "Entitas tidak ditemukan!";
	}
	else{
		$query_operation = "DELETE FROM entitas WHERE kode_entitas='".$_kode_entitas."' ";
		$message = "Entitas berhasil dihapus!";
		$tpl->insert_log("hapus entitas ".$_kode_entitas,$query_operation);
	}
	
}
//echo $query_operation;
if($error!=""){
	$tpl->alert_panel("Validasi Form!",$error,"");
}
else{
	
	$exec = $db->Execute($query_operation);
	if(!$exec){
		$error = "Periksa kembali inputan anda!";
	}
	
	
	if($error!=""){
		$tpl->alert_panel("Validasi Form!",$error,"");
	}
	else{
		$tpl->alert_panel("Informasi",$message,$button,"info");	
	}
}

?>