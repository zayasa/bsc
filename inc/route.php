<?php

$controller = $url->get_url(array('controller'));
if(isset($controller)) {
	switch ($controller) {
	   case "home" :
			$navv = "aberanda" ;
			$breadnav = array("beranda");
			$file[] = "query/home.php";
			$file[] = "view/home.php";
			break;
		   				
		
		/* validasi */
		case "check_password" :
			$file[] = "ajax/check_password.php";
			break;
			
	
		/* pengguna */
		case "profil-saya" :
			$navv = "profil-saya" ;
			$breadnav = array("beranda","profil saya");
			
			
			$action = $url->get_url(array('action'));
			if($action=="update"){
				$file[] = "action/update_profil.php";
			}
			else{
				$file[] = "view/profil.php";				
			}
			break;
		
		
		/* scorecard */
	   case "daftar-pekerjaan" :
			$breadnav = array("beranda","daftar pekerjaan");
			$action = $url->get_url(array('action'));
			
			if($action=="update" || $action=="simpan" || $action=="hapus"){

				$file[] = "action/approve_permohonan.php";				
			}
			else{
				$file[] = "query/daftar_pekerjaan.php";
				$file[] = "view/daftar_pekerjaan.php";
			}
	   
			break;
	   case "periode-scorecard" :
			$navv = "aperiode-scorecard" ;
			$breadnav = array("beranda","scorecard","periode scorecard");
			
			
			$action = $url->get_url(array('action'));
			
			if($action=="update" || $action=="simpan" || $action=="hapus"){
				$file[] = "action/periode_scorecard.php";				
			}
			else if($action=="ubah" || $action=="tambah"){
				$file[] = "query/detail_periode_scorecard.php";
				$file[] = "view/form_periode_scorecard.php";
			}
			else{
				$file[] = "query/periode_scorecard.php";
				$file[] = "view/periode_scorecard.php";
				
			}
						
			break;
		   				   		   				   
	   case "waktu-periode-scorecard" :
			$navv = "aperiode-scorecard" ;
			$breadnav = array("beranda","scorecard","periode scorecard","waktu");
			
			
			$kode_periode = $url->get_url(array('kode_periode'));
			$kode_satuan_waktu = $url->get_url(array('kode_satuan_waktu'));
			$action = $url->get_url(array('action'));
			
			if($action!="" && $action=="atur" && $kode_satuan_waktu!=""){
				$file[] = "action/waktu_periode_scorecard.php";				
			}
			else if($kode_satuan_waktu!=""){
				$file[] = "query/detail_waktu_periode_scorecard.php";
				$file[] = "view/form_waktu_periode_scorecard.php";
			}
			else{
				$file[] = "query/waktu_periode_scorecard.php";
				$file[] = "view/waktu_periode_scorecard.php";
				
			}
						
			break;
		   				   		   				   
	   case "scorecard-unit-kerja" :
			$navv = "ascorecard-unit-kerja" ;
			$breadnav = array("beranda","scorecard","scorecard unit kerja");
			$kode_periode = $url->get_url(array('kode_periode'));
			$kode_unit_kerja = $url->get_url(array('kode_unit_kerja'));
			$action = $url->get_url(array('action'));
			
			if($kode_periode!="" && $kode_unit_kerja!="" && $action=="simpan-nilai"){	
				$file[] = "action/nilai_scorecard.php";
			}
			else if($kode_periode!="" && $kode_unit_kerja!="" && $action=="nilai"){
				$file[] = "query/detail_nilai_scorecard.php";
				$file[] = "view/form_nilai_scorecard.php";
			}
			else if($kode_periode!="" && $kode_unit_kerja!="" && $action=="daftar-nilai"){
				$file[] = "query/nilai_scorecard.php";
				$file[] = "view/daftar_nilai_scorecard.php";
			}
			else if($kode_periode!="" && $kode_unit_kerja!="" && ($action=="simpan" || $action=="update" )){
				$file[] = "action/scorecard.php";
			}
			else if($kode_periode!="" && $kode_unit_kerja!="" && ($action=="tambah" || $action=="ubah")){
				$file[] = "query/detail_scorecard.php";
				$file[] = "view/form_scorecard.php";
			}
			else if($kode_periode!=""){
				$file[] = "query/scorecard_unit_kerja.php";
				$file[] = "view/scorecard_unit_kerja.php";
			}
			else{				
				$file[] = "query/daftar_periode_scorecard.php";
				$file[] = "view/daftar_periode_scorecard.php";
			}
						
			break;
		   				   		   				   
	   case "scorecard-pegawai" :
			$navv = "ascorecard-pegawai" ;
			$breadnav = array("beranda","scorecard","scorecard pegawai");
			$kode_periode = $url->get_url(array('kode_periode'));
			$kode_pengguna = $url->get_url(array('kode_pengguna'));
			$action = $url->get_url(array('action'));
			
			$kode_unit_kerja = $kode_pengguna;
			if($kode_periode!="" && $kode_pengguna!="" && $action=="simpan-nilai"){	
				$file[] = "action/nilai_scorecard.php";
			}
			else if($kode_periode!="" && $kode_pengguna!="" && $action=="nilai"){
				$file[] = "query/detail_nilai_scorecard.php";
				$file[] = "view/form_nilai_scorecard.php";
			}
			else if($kode_periode!="" && $kode_pengguna!="" && $action=="daftar-nilai"){
				$file[] = "query/nilai_scorecard.php";
				$file[] = "view/daftar_nilai_scorecard.php";
			}
			else if($kode_periode!="" && $kode_pengguna!="" && ($action=="simpan" || $action=="update" )){
				$file[] = "action/scorecard.php";
			}
			else if($kode_periode!="" && $kode_pengguna!="" && ($action=="tambah" || $action=="ubah")){
				$file[] = "query/detail_scorecard.php";
				$file[] = "view/form_scorecard.php";
			}
			else if($kode_periode!="" && $kode_pengguna!=""){
				$file[] = "query/detail_scorecard_pengguna.php";
				$file[] = "view/detail_scorecard_pengguna.php";				
			}
			else if($kode_periode!=""){
				$file[] = "query/daftar_pengguna.php";
				$file[] = "view/daftar_pengguna.php";				
			}
			else{				
				$file[] = "query/daftar_periode_scorecard.php";
				$file[] = "view/daftar_periode_scorecard.php";
			}
			break;			   
		   				   
		/* laporan */
	   case "ringkasan-scorecard" :
			$navv = "aringkasan-scorecard" ;
			$breadnav = array("beranda","laporan","ringkasan scorecard");
			$kode_periode = $url->get_url(array('kode_periode'));
			$level = $url->get_url(array('level'));
			$kode_induk = $url->get_url(array('kode_induk'));
			
			if($action=="tampil"){
				$file[] = "query/laporan_unit_kerja.php";
				$file[] = "view/hasil_laporan_unit_kerja.php";				
			}
			else{
				$file[] = "query/ringkasan_scorecard.php";
				$file[] = "view/ringkasan_scorecard.php";				
			}
			break;			   
		   				   		   
	   case "laporan-scorecard" :
			$navv = "alaporan-scorecard" ;
			$breadnav = array("beranda","laporan","laporan scorecard");
			$action = $url->get_url(array('action'));
			
			if($action=="tampil"){
				$file[] = "query/laporan_scorecard.php";
				$file[] = "view/hasil_laporan_scorecard.php";				
			}
			else{
				$file[] = "view/laporan_scorecard.php";				
			}
			break;			   
		   				   		   				   
	   case "laporan-unit-kerja" :
			$navv = "alaporan-unit-kerja" ;
			$breadnav = array("beranda","laporan","laporan unit kerja");
			$action = $url->get_url(array('action'));
			
			if($action=="tampil"){
				$file[] = "query/laporan_unit_kerja.php";
				$file[] = "view/hasil_laporan_unit_kerja.php";				
			}
			else{
				$file[] = "view/laporan_unit_kerja.php";				
			}
			break;			   

	   case "laporan-pegawai" :
			$navv = "alaporan-pegawai" ;
			$breadnav = array("beranda","laporan","laporan pegawai");
			$action = $url->get_url(array('action'));
			
			if($action=="tampil"){
				$file[] = "query/laporan_pegawai.php";
				$file[] = "view/hasil_laporan_pegawai.php";				
			}
			else{
				$file[] = "view/laporan_pegawai.php";				
			}
						
			break;			   
			
		/* pengaturan */
	   case "entitas" :
			$navv = "aentitas" ;
			$breadnav = array("beranda","pengaturan","entitas");
			
			
			$action = $url->get_url(array('action'));
			
			if($action=="update" || $action=="simpan" || $action=="hapus"){
				$file[] = "action/entitas.php";				
			}
			else if($action=="ubah" || $action=="tambah"){
				$file[] = "query/detail_entitas.php";
				$file[] = "view/form_entitas.php";
			}
			else{
				$file[] = "query/entitas.php";
				$file[] = "view/entitas.php";
				
			}
						
			break;			   
		   				   		   				   
	   case "unit-kerja" :
			$navv = "aunit-kerja" ;
			$breadnav = array("beranda","pengaturan","unit kerja");
			
			$action = $url->get_url(array('action'));
			
			if($action=="update" || $action=="simpan" || $action=="hapus"){
				$file[] = "action/unit_kerja.php";				
			}
			else if($action=="ubah" || $action=="tambah"){
				$file[] = "query/detail_unit_kerja.php";
				$file[] = "view/form_unit_kerja.php";
			}
			else{
				$file[] = "query/unit_kerja.php";
				$file[] = "view/unit_kerja.php";
				
			}
						
			break;			   
		   				   		   				   
	   case "jabatan" :
			$navv = "ajabatan" ;
			$breadnav = array("beranda","pengaturan","jabatan");
			
			$action = $url->get_url(array('action'));
			
			if($action=="update" || $action=="simpan" || $action=="hapus"){
				$file[] = "action/jabatan.php";				
			}
			else if($action=="ubah" || $action=="tambah"){
				$file[] = "query/detail_jabatan.php";
				$file[] = "view/form_jabatan.php";
			}
			else{
				$file[] = "query/jabatan.php";
				$file[] = "view/jabatan.php";
				
			}
						
			break;			   

	   case "pengguna" :
			$navv = "apengguna" ;
			$breadnav = array("beranda","pengaturan","pengguna");
			
			$action = $url->get_url(array('action'));
			
			if($action=="update" || $action=="simpan" || $action=="hapus" || $action=="reset"){
				$file[] = "action/pengguna.php";				
			}
			else if($action=="ubah" || $action=="tambah"){
				$file[] = "query/detail_pengguna.php";
				$file[] = "view/form_pengguna.php";
			}
			else{
				$file[] = "query/pengguna.php";
				$file[] = "view/pengguna.php";				
			}
						
			break;			   

	   case "hak-akses" :
			$navv = "ahak-akses" ;
			$breadnav = array("beranda","pengaturan","hak akses");
			
			$action = $url->get_url(array('action'));
			
			if($action=="update" || $action=="simpan" || $action=="hapus" ){
				$file[] = "action/hak_akses.php";				
			}
			else if($action=="ubah" || $action=="tambah"){
				$file[] = "query/detail_hak_akses.php";
				$file[] = "view/form_hak_akses.php";
			}
			else{
				$file[] = "query/hak_akses.php";
				$file[] = "view/hak_akses.php";				
			}
						
			break;			   

	   case "akses-pegawai" :
			$navv = "aakses-pegawai" ;
			$breadnav = array("beranda","pengaturan","hak akses pegawai");
			$file[] = "view/home.php";
			break;			   

		default :
			$navv = "aberanda" ;
			$bread_nav = "Home" ;
			$file[] = "query/home.php";
			$file[] = "view/home.php";
	}
}
else {
	$navv = "aberanda" ;
	$bread_nav = "Home" ;
	$file[] = "query/home.php";
	$file[] = "view/home.php";
}
?>