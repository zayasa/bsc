<?php
ob_start();
session_start();

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

define("DOCUMENT_ROOT",$_SERVER['DOCUMENT_ROOT']);
define("DEVELOPMENT",0);
define("DATA_DISPLAYED",10);
$list_status_pengguna = array("0"=>"non-aktif","1"=>"aktif","2"=>"ditangguhkan");
$array_menu = array(
					"aberanda" => "home",
					"ascorecard" => "",
					"aperiode-scorecard" => "periode-scorecard",
					"ascorecard-unit-kerja" => "scorecard-unit-kerja",
					"ascorecard-pegawai" => "scorecard-pegawai",
					"alaporan" => "",
					"aringkasan-scorecard" => "ringkasan-scorecard",
					"alaporan-scorecard" => "laporan-scorecard",
					"alaporan-unit-kerja" => "laporan-unit-kerja",
					"alaporan-unit-kerja" => "laporan-unit-kerja",
					"alaporan-pegawai" => "laporan-pegawai",
					"alaporan-pegawai" => "laporan-pegawai",
					"apengaturan" => "",
					"aentitas" => "entitas",
					"aunit-kerja" => "unit-kerja",
					"ajabatan" => "jabatan",
					"apengguna" => "pengguna",
					"ahak-akses" => "hak-akses"
				);



/* define connection */
$dbhostname		= "127.0.0.1";
$dbusername		= "root";
$dbpassword		= "";
$dbname			= "bsc";


require_once(DOCUMENT_ROOT."/classes/adodb/adodb.inc.php");

$db = ADONewConnection("mysqli");
$db->PConnect($dbhostname, $dbusername, $dbpassword, $dbname);
$db->SetFetchMode(ADODB_FETCH_ASSOC);


require_once(DOCUMENT_ROOT."/classes/global.php");
require_once(DOCUMENT_ROOT."/classes/template.php");
$tpl = new app_global();


require_once(DOCUMENT_ROOT."/classes/url.php");
$url = new url();


function is_decimal($val){
    return is_numeric( $val ) && floor( $val ) != $val;
}
?>