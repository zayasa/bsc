<?php
$kode_scorecard = $url->get_url(array('kode_scorecard'));

/* get pilihan tipe nilai */
$array_tipe_penilaian = array();
$array_kelompok_penilaian = array();
$q_daftar_tipe = "SELECT * FROM tipe_nilai ";
$r_daftar_tipe = $db->Execute($q_daftar_tipe);
while($daftar_tipe = $r_daftar_tipe->fetchRow()){
	foreach($daftar_tipe AS $key=>$val){
		$key  = strtolower($key);
		$$key = trim($val);
	}
	$array_tipe_penilaian[$tipe_nilai] = $nama_tipe_nilai;
	$array_kelompok_penilaian[$tipe_nilai] = $kelompok_nilai;
}


$query_detail_scorecard = "SELECT * FROM scorecard AS a LEFT JOIN polarisasi_scorecard AS b ON a.tipe_polarisasi_scorecard=b.tipe_polarisasi_scorecard LEFT JOIN satuan_waktu AS c ON a.satuan_waktu_scorecard=c.kode_satuan_waktu LEFT JOIN satuan_nilai AS d ON a.satuan_nilai_scorecard=d.kode_satuan_nilai LEFT JOIN tipe_scorecard AS e ON a.tipe_scorecard=e.kode_tipe_scorecard WHERE kode_periode_scorecard='".$kode_periode."' AND kode_pemilik_scorecard='".$kode_unit_kerja."' AND kode_scorecard='".$kode_scorecard."' ";
$rs_detail_scorecard = $tpl->get_last_record($query_detail_scorecard);
if(is_array($rs_detail_scorecard)){
	foreach($rs_detail_scorecard AS $key=>$val){
		$key  = strtolower($key);
		
		if(preg_match("/tanggal|tgl/i",$key) && $val!="")
			$val = date("Y-m-d",strtotime($val));
		
		$$key = trim($val);
	}
	$readonly = "readonly";
	$_action = "simpan-nilai";
}

if($detail_login['kode_hak_akses_pengguna']!="admin")
	$cond = "AND SYSDATE() BETWEEN tanggal_mulai AND tanggal_selesai ";
else 
	$cond = "";

/* get range periode aktif */
$q_periode = "SELECT a.*,_TAR,_RAL,_MIN,_MAX,_BSE,_SCO FROM periode_waktu AS a LEFT JOIN v_scorecard_nilai_periode AS b ON a.id=b.id_periode_waktu AND '".$kode_scorecard."'=b.kode_scorecard WHERE a.kode_periode='".$kode_periode."' AND a.kode_satuan_waktu='".$satuan_waktu_scorecard."' ".$cond;
$l_periode = $db->Execute($q_periode);
$rs_periode = $l_periode->getArray();

$q_label_nilai = "SELECT * FROM label_nilai WHERE kode_label_nilai='".$label_nilai_scorecard."' ";
$arr_label_nilai = $tpl->get_last_record($q_label_nilai);
?>