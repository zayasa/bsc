<?php
$kode_scorecard = $url->get_url(array('kode_scorecard'));
$id_periode_waktu = $url->get_url(array('id_periode_waktu'));


/* get pilihan tipe nilai */
$array_tipe_penilaian = array();
$array_kelompok_penilaian = array();
$q_daftar_tipe = "SELECT * FROM tipe_nilai ";
$r_daftar_tipe = $db->Execute($q_daftar_tipe);
while($daftar_tipe = $r_daftar_tipe->fetchRow()){
	foreach($daftar_tipe AS $key=>$val){
		$key  = strtolower($key);
		$$key = trim($val);
	}
	$array_tipe_penilaian[$tipe_nilai] = $nama_tipe_nilai;
	$array_kelompok_penilaian[$tipe_nilai] = $kelompok_nilai;
}


$query_detail_scorecard = "SELECT * FROM scorecard AS a LEFT JOIN polarisasi_scorecard AS b ON a.tipe_polarisasi_scorecard=b.tipe_polarisasi_scorecard LEFT JOIN satuan_waktu AS c ON a.satuan_waktu_scorecard=c.kode_satuan_waktu LEFT JOIN satuan_nilai AS d ON a.satuan_nilai_scorecard=d.kode_satuan_nilai LEFT JOIN tipe_scorecard AS e ON a.tipe_scorecard=e.kode_tipe_scorecard WHERE kode_periode_scorecard='".$kode_periode."' AND kode_pemilik_scorecard='".$kode_unit_kerja."' AND kode_scorecard='".$kode_scorecard."' ";
$rs_detail_scorecard = $tpl->get_last_record($query_detail_scorecard);
if(is_array($rs_detail_scorecard)){
	foreach($rs_detail_scorecard AS $key=>$val){
		$key  = strtolower($key);
		
		if(preg_match("/tanggal|tgl/i",$key) && $val!="")
			$val = date("Y-m-d",strtotime($val));
		
		$$key = trim($val);
	}
	$readonly = "readonly";
	$_action = "simpan-nilai";
}


/* get range periode aktif */
$array_nilai = array();
$q_periode = "SELECT * FROM scorecard_nilai AS a WHERE a.kode_scorecard='".$kode_scorecard."' AND a.id_periode_waktu='".$id_periode_waktu."'";
$l_periode = $db->Execute($q_periode);
while($periode = $l_periode->fetchRow()){
	foreach($periode AS $key=>$val){
		$key  = strtolower($key);		
		if(preg_match("/tanggal|tgl/i",$key) && $val!="")
			$val = date("Y-m-d",strtotime($val));
		
		$$key = trim($val);
	}
	
	$array_nilai[$tipe_nilai_scorecard] =  $periode;
}

/* tentukan tipe inputan nilai */
if($tipe_satuan_nilai=="date")
	$type_input ="date";
else 
	$type_input ="text";
	
?>